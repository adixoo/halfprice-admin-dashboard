import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-sport',
  templateUrl: './create-sport.component.html',
  styleUrls: ['./create-sport.component.scss']
})
export class CreateSportComponent implements OnInit {


  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'icon',
          type: 'image',
          placeholder: 'Upload  Icon',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
            
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      submitApi: {
        apiPath: `/sports`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Sports created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
