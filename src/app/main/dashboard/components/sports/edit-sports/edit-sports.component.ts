import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-sports',
  templateUrl: './edit-sports.component.html',
  styleUrls: ['./edit-sports.component.scss']
})
export class EditSportsComponent implements OnInit {


  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() sportId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'icon',
          type: 'image',
          placeholder: 'Upload  Icon',
          value:'icon',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
            
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/sports/${this.sportId}`,
        method:'GET'
      },
      apiDataKey: 'data',
      submitApi: {
        apiPath: `/sports/${this.sportId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Sport updated successfully!"
    }
  }


  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
