import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-collection',
  templateUrl: './edit-collection.component.html',
  styleUrls: ['./edit-collection.component.scss']
})
export class EditCollectionComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() collectionId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },      
        // {
        //   name: 'banners',
        //   type: 'image',
        //   placeholder: 'Upload Banner Image',
        //   value:'banners',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 2,
        //     multipleSelect: true,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
      
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/collections/${this.collectionId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/collections/${this.collectionId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Collections updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
