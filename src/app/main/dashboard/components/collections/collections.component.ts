import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateCollectionComponent } from './create-collection/create-collection.component';
import { EditCollectionComponent } from './edit-collection/edit-collection.component';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() {
    // Fetching Path Params
  }

  ngOnInit() {
    // Table Data Init
    this.tableConfig = {
      discriminator: "tableConfig",
      displayedColumns: [
        {
          header: "name",
          type: "text",
          value: "name",
        },
        {
          header: "Status",
          type: "enum_icon",
          value: "status",
          enumIcons: [
            {
              value: "active",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Active",
                color: "#10d817",
              },
            },
            {
              value: "inactive",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Inactive",
                color: "#ee3f37",
              },
            },
          ],
        },
      ],
      fieldActions: [
        {
          action: {
            actionType: "modal",
            modal: {
              component: EditCollectionComponent,
              inputData: [
                {
                  inputKey: "collectionId",
                  type: "key",
                  key: "id",
                },
              ],
              header: "Edit collection",
              width: 600,
            },
            
          },
          icon: {
            name: "edit",
            tooltip: "Edit Product",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/collections/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "active",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to active collections?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "inactive" },
            },
          },
          icon: {
            type: CanIconType.Material,
            name: "thumb_up_al",
            tooltip: "Change Status",
            color: "#10d817",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/collections/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "inactive",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to inactive collections?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "active" },
            },
          },
          icon: {
            // name: 'edit',
            // tooltip: 'change status',
            type: CanIconType.Material,
            name: "thumb_down_al",
            tooltip: "Change Status",
            color: "#ee3f37",
          },
        }
      ],
      api: {
        apiPath: "/collections",
        method: "GET"
      },
      countApi: {
        apiPath: "/collections/count",
        method: "GET"
      },
      countApiDataKey: "data.count",
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: "Collections",
    };

    // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: "add",
        tooltip: "Create New",
      },
      type: "modal",
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateCollectionComponent,
        inputData: [],
        width: 600,
        header: "Add Collections",
      },
     
    };
  }

}
