import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-unit-details',
  templateUrl: './unit-details.component.html',
  styleUrls: ['./unit-details.component.scss']
})
export class UnitDetailsComponent implements OnInit {

  public detailViewData: CanDetailView;
  @Input() unitId: any;
  dataSource : any

  constructor() { }

  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      dataSource:this.dataSource,
      labelPosition: 'inline',
      displayedFields: [
        {
          header: 'Name',
          type: 'text',
          value: 'name',
        },
        {
          header: 'Description',
          type: 'text',
          value: 'description',
        },
        {
          header:"Project Name",
          type :'text',
          value :'project.name'
        },
        {
          header:"Builder Name",
          type :'text',
          value:"builder.name"
        },
        {
          header:'Tower',
          type :'text',
          value :"tower.name"
        },
        {
          header:'rooms',
          type :'text',
          value:'rooms',
          keySeparators:[","]
        }
        
      ],
 
      // header: "Master Products Details"
    }
  }

}
