import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateSubCategoryComponent } from './create-sub-category/create-sub-category.component';
import { EditSubCategoryComponent } from './edit-sub-category/edit-sub-category.component';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
 
  @Input() categoryId :  any;
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor(private activedRoute: ActivatedRoute) { 
    this.activedRoute.paramMap.subscribe(paramMap => this.categoryId = parseInt(paramMap.get('id')));
  }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          },
          // {
          //   header: 'Banner',
          //   type: 'image',
          //   images: {
          //     showAll: true,
          //     openType: 'modal',
          //     imageItems: [
          //       {
          //         type: 'api',
          //         isArray: true,
          //         alt: 'Preview',
          //         value: 'banners'
          //       }
          //     ]
          //   }
          // },
          // {
          //   header: 'Icon',
          //   type: 'image',
          //   images: {
          //     showAll: true,
          //     openType: 'modal',
          //     imageItems: [
          //       {
          //         type: 'api',
          //         isArray: false,
          //         alt: 'Preview',
          //         value: 'icon'
          //       }
          //     ]
          //   }
          // },
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditSubCategoryComponent,
                inputData: [
                  {
                    inputKey: 'subCategoryId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Sub Category',
                width: 600
              }
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Sub Category',
            }
          }
        ],
        filters: [
          {
            filtertype: 'local',
            placeholder: 'Search',
            keys: ['name']
          }
        ],
        api: {
          apiPath: '/sub-categories',
          method: 'GET',
          params : new HttpParams().set('categoryId',this.categoryId)
        },
        countApi: {
          apiPath: '/sub-categories',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Sub Categories'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateSubCategoryComponent,
        inputData: [
          {
            inputKey: "categoryId",
            type: 'fixed',
            value: this.categoryId
          }
        ],
        width: 600,
        header: 'Add Sub Category'
      },
    }
  }


}
