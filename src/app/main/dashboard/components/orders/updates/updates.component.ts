import { HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanCardView } from 'src/@can/types/card-view.type';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanPermission } from 'src/@can/types/permission.type';
import { CanButton } from 'src/@can/types/shared.type';
import { PostUpdatesComponent } from './post-updates/post-updates.component';
import { UpdateService } from './update.service';
// import { MatDialog, MatDialogRef } from 'dlv-material';

@Component({
  selector: 'app-updates',
  templateUrl: './updates.component.html',
  styleUrls: ['./updates.component.scss']
})
export class UpdatesComponent implements OnInit {
  

  constructor( 
     private apiService: CanApiService, 
     private updateService : UpdateService,
     public dialog: MatDialog ) { }

dialogRef: MatDialogRef<PostUpdatesComponent, any>; 
  @Input() order:any;
  public progresses : any;
  public cardViewConfig: CanCardView;
  public detailViewData: CanDetailView;
  public postUpdatesButtonConfig : CanButton;
  public postUpdatePermission: CanPermission;


 async ngOnInit() {
  this.postUpdatesButtonConfig = {
    label : 'Post an update',
    type : 'stroked',
    color : 'primary',
    float:'left'
  }
  this.postUpdatePermission = { type: 'single', match: { key: 'UPDATE_ORDERS', value: true }}

    // await this.getPrograss();
       // Detail View Config Init
      //  this.detailViewData = {
      //   discriminator: 'detailViewConfig',
      //   columnPerRow: 1,
      //   labelPosition: 'inline',
      //   // dataSource:this.order,
      //   displayedFields: [
      //     {
      //       header: null,
      //       type: 'text',
      //       value: 'remarks',
      //     },
      //     {
      //       header: "Status",
      //       type: 'text',
      //       value: 'status',
      //     },
      //     {
      //       header:"Date",
      //       type :'date',
      //       value :'createdAt'
      //     }
          
      //   ],
      //   api:{
      //     apiPath: `/progresses`,
      //     method: 'GET',
      //     params :new HttpParams().set('orderId',this.order.id)
      //   }
      // }
    //  this.cardViewConfig = {
    //   columnPerRow: 3,
    //   type: 'default',
    //   title: {
    //     valueType: 'key',
    //     key: 'name',
    //     type:'text'
    //   },
    //   subTitle: {
    //     valueType: 'key',
    //     key: 'email',
    //     type: 'text'
    //   },
    //   content: {
    //     valueType: 'key',
    //     key: 'mobile',
    //     type:'text'
    //   },
    //   isArray: false,
    //   maxContentLines:1,
    //   api: {
    //     apiPath: `/progresses`,
    //     method: 'GET',
    //     params :new HttpParams().set('orderId',this.order.id)
    //   }
    // }

  }

  openPostModal(){
    // this.notificationService.showSuccess();
    const dialogRef = this.dialog.open(PostUpdatesComponent,
      {
        width: '700px',
        data: { order: this.order }
      }
    );
    dialogRef.afterClosed().subscribe(submitResponse => {
    });
    // return new Promise((resolve, reject) => {
    //   const dialogRef = this.dialog.open(PostUpdatesComponent, {
    //     disableClose: true,
    //     data: [{

    //     }]
    //   });
    //   dialogRef.afterClosed().subscribe((dialogResult) => {
    //     resolve(dialogResult);
    //   });
    // })
  }

  // getPrograss(){
  //   return new Promise((resolve, reject) => {
  //     const getProperties: CanApi = {
  //       apiPath: `/progresses`,
  //       method: 'GET',
  //       params :new HttpParams()
  //       .append('include',JSON.stringify({all : true}))
  //       // .append('order', JSON.stringify(['createdAt DESC']))
  //       .set('status','active')
  //       .set('orderId',this.order.id)
  //     };
  //     this.apiService.request(getProperties).subscribe((res: any[]) => {
  //       this.progresses = res;
  //       this.ref.detectChanges();
  //       resolve(res);
  //     });
  //   });
  // }

  outputEvent(event){
    this.updateService.isFormSubmitted = true;
  }
}
