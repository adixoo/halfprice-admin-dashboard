import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CanCardView } from 'src/@can/types/card-view.type';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { RepresentativeComponent } from '../../representative/representative.component';
import { UpdateService } from '../update.service';

@Component({
  selector: 'app-previous-updates',
  templateUrl: './previous-updates.component.html',
  styleUrls: ['./previous-updates.component.scss']
})
export class PreviousUpdatesComponent implements OnInit {

  constructor(private updateService : UpdateService) { }
  // @Input() progress:any;
  @Input() order:any;
  public detailViewData: CanDetailView;
  cardViewConfig: CanCardView;
  refreshEvent = new EventEmitter<boolean>(true);
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
    
    this.cardViewConfig = {
      columnPerRow: 3,
      type: 'default',
      refreshEvent : this.refreshEvent,
      // dataSource: this.progress,
      title: {
        valueType: 'key',
        // value: this.progress.status,
        type:'text',
        key:'createdBy.firstName createdBy.middleName createdBy.lastName',
        keySeparators:[' ']
      },
      subTitle: {
        valueType: 'key',
        key: 'remarks',
        type: 'text',
      },
      content: {
        valueType: 'dynamic',
        dynamicText: 'PROGRESS: ${progress} \n\nORDER STATUS: ${progressStatus}\n\nDATE: ${createdAt->DD/MM/YYYY HH:mm:ss}',
        // type:'date',
        // displayDateType : 'dd-MMM-yyyy'
        
      },
      maxContentLines: 4,
      image: {
        maxHeight:200,
        images: [{
          valueType: 'key',
          key: 'images',
          type:'image',
          isArray: true
        }]
      },
      isArray: true,
      actionButtons: [
        {
          action: {
            actionType: 'ajax',
            api :{
              apiPath :'/progresses/${id}',
              method:'PATCH'
            },
            bodyParams:[
              {
                key: 'status',
                value: 'inactive'
              },
            ],
            confirm: {
              title: 'Hide',
              message: 'Are you sure you want to hide?',
              buttonText: {
                confirm: 'Confirm',
                cancel: 'Cancel'
              }
            },
          },
          button:{
            type:'stroked',
            label:"Hide",
            color:'primary'
          }
        },
      ],
      api:{
       apiPath: `/progresses`,
       method: 'GET',
       params :new HttpParams()
       .append('include',JSON.stringify({all : true}))
       .set('status','active')
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
       .set('orderId',this.order.id)
    }

    }

    this.updateService.isFormSubmitted$.subscribe(value => {
      this.refreshEvent.next(value)
    })
    this.updateService.isTransactionAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })
    
              // Fab Button Config
          this.fabButtonConfig = {
            icon: {
              type: 1,
              name: 'person_add',
              tooltip: 'Assign Representative'
            },
            type: 'modal',
            position: CanFabButtonPosition.BottomRight,
            modal: {
              component: RepresentativeComponent,
              inputData: [
                {
                  inputKey: "orderId",
                  type: 'fixed',
                  value: this.order.id
                }
              ],
              width: 600,
              header: 'Assign Representative'
            },
            // permission: { type: 'single', match: { key: 'UPDATE_ORDERS', value: true }}
            
            
          };

  }

}
