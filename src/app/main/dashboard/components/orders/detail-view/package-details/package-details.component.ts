import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss']
})
export class PackageDetailsComponent implements OnInit {

  public tableConfig: CanTable;
  @Input() dataSource:any;

  constructor() { }

  ngOnInit() {
       // Table Data Init
       this.tableConfig = {
        discriminator: 'tableConfig',
        dataSource : this.dataSource,
        hideRefreshButton:true,
        displayedColumns: [
          {
            header: 'SKU',
            type: 'text',
            value: 'internalSku'
          },
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header:"Quantity",
            type :'text',
            value :"quantity"
          },
          {
            header:'Brand',
            type :'text',
            value :'brandName'
          },
          {
            header: 'Unit Price',
            type: 'text',
            value: 'sellingPrice',
          },
          {
            header :'Vendor',
            type :'text',
            value : 'vendor.name'
          }
        ],
        fieldActions: [
    
          // {
          //   action: {
          //     actionType: 'modal',
          //     modal: {
          //       component: EditConsumerComponent,
          //       inputData: [
          //         {
          //           inputKey: 'userId',
          //           type: 'key',
          //           key: 'id'
          //         }
          //       ],
          //       header: 'Edit Transactions',
          //       width: 600
          //     }
          //   },
          //   icon: {
          //     name: 'edit',
          //     tooltip: 'Edit Transactions',
          //   }
          // },
        ],
        // api: {
        //   apiPath: '/transactions',
        //   method: 'GET',
        //   params : new HttpParams().set('orderId', this.order.id)
        // },
        // countApi: {
        //   apiPath: '/transactions/count',
        //   method: 'GET',
        //   params : new HttpParams().set('orderId', this.order.id)
  
        // },
        // countApiDataKey: 'count',
        // pagination: {
        //   pageSizeOptions: [100, 200]
        // },
        // header: 'Transactions'
      }
  }

}
