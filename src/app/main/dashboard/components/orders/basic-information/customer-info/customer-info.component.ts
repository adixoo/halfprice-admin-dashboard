import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent implements OnInit {

  public detailViewData: CanDetailView;
  @Input() order: any;
  constructor() { }

  ngOnInit() {
       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        labelPosition: 'top',
        displayedFields: [
          {
            header: '',
            type: 'text',
            value: 'firstName middleName lastName',
            keySeparators:[' ']
          },
          {
            header: '',
            type: 'text',
            value: 'email',
          },
          {
            header: '',
            type: 'text',
            value: 'mobile',
          }
        ],
      api:{
        apiPath: `/users/${this.order.userId}`,
        method:'GET'
      }
      }
  }
}
