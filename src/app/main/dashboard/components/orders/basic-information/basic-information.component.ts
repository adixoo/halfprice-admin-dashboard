import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { ApiType, CanApi } from 'src/@can/types/api.type';
import { CanCardView } from 'src/@can/types/card-view.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-basic-information',
  templateUrl: './basic-information.component.html',
  styleUrls: ['./basic-information.component.scss']
})
export class BasicInformationComponent implements OnInit {

  @Input() order:any
  public customercardViewConfig: CanCardView;
  public orderInfoViewConfig: CanCardView;
  public tableConfig: CanTable;
  public orderItems:any;

  // public order : any;

  constructor(
    private apiService: CanApiService
    ) { }

    async ngOnInit() {
      await this.getOrderItems();
     }
  
    
  
   getOrderItems(){
    return new Promise((resolve) => {
      const orderApi: CanApi = {
        apiPath: `/orders/products?id=${this.order.id}`,
        method: 'GET',
        // params: new HttpParams()
        // .append('id',this.order.id)
      }
      this.apiService.request(orderApi)
        .subscribe((items:any[]) => {
          this.orderItems = items;
          })
    })
  }
  // getOrder(){
  //   return new Promise((resolve, reject) => {
  //     const getProperties: CanApi = {
  //       apiPath: `/orders/${this.orderId}`,
  //       method: "GET",
  //     };
  //     this.apiService.request(getProperties).subscribe((res: any[]) => {
  //       this.order = res;
  //       resolve(res);
  //     });
  //   });
  // }
 
}
