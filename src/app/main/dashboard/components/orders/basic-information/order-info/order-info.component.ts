import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { UpdateService } from '../../updates/update.service';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent implements OnInit {

  public detailViewData: CanDetailView;
  refreshEvent = new EventEmitter<boolean>(true);
  @Input() order: any;
  private subscription:Subscription;

  constructor(private updateService : UpdateService) { }

 
  ngOnInit() {
     this.subscription =  this.updateService.isTransactionAdded$.subscribe(value => {
       this.refreshEvent.next(value)
      })
      this.updateService.isRepresentativeAssigned$.subscribe(value => {
        this.refreshEvent.next(value)
      })

       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        labelPosition: 'inline',
        refreshEvent: this.refreshEvent,
        // dataSource:this.order,
        displayedFields: [
          {
            header: 'Date',
            type: 'date',
            value: 'createdAt',
          },
          {
            header: 'Completion Date',
            type: 'date',
            value: 'updatedAt',
            displayCondition:{
              type :'single',
              match:{key :'status', operator:'equals' , value:'completed'}
            }
          },
          {
            header:"Status",
            type :'text',
            value :'status'
          },
          {
            header:"Amount",
            type :'text',
            value:"totalAmount"
          },
          {
            header:"Payment",
            type :'text',
            value:"paymentStatus"
          },
          {
            header:'Assigned To',
            type :'text',
            value :"representative.name"
          }
          
        ],
        api:{
          apiPath: `/orders/${this.order.id}`,
          method:'GET',
          params: new HttpParams().append('include',JSON.stringify([{all:true}]))
        }
      }
  }

  // ngOnDestroy(){
  //   if(this.subscription && !this.subscription.closed){
  //     this.subscription.unsubscribe();
  //   }
  // }

}
