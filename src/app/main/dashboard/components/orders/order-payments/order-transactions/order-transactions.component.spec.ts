import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTransactionsComponent } from './order-transactions.component';

describe('OrderTransactionsComponent', () => {
  let component: OrderTransactionsComponent;
  let fixture: ComponentFixture<OrderTransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderTransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
