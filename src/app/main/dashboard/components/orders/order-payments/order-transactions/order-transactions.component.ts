import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CanTable } from 'src/@can/types/table.type';
import { UpdateService } from '../../updates/update.service';

@Component({
  selector: 'app-order-transactions',
  templateUrl: './order-transactions.component.html',
  styleUrls: ['./order-transactions.component.scss']
})
export class OrderTransactionsComponent implements OnInit {

  @Input() order:any;
  constructor(private updateService : UpdateService) { }
  public tableConfig: CanTable;
  public paidAmount:number = 0;
  refreshEvent = new EventEmitter<boolean>(true);
  private subscription:Subscription;
  ngOnInit() {
    this.subscription =  this.updateService.isTransactionAdded$.subscribe(value =>{
      this.refreshEvent.next(value)
    })
  // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      refreshEvent : this.refreshEvent,
      displayedColumns: [
        {
          header: 'Order Number',
          type: 'text',
          value: 'orderNumber'
        },
        {
          header: 'Platform',
          type: 'text',
          value: 'platform',
        },
        {
          header:'Amount',
          type:'text',
          value:'amount'
        },
        {
          header: 'pgTransaction Id',
          type: 'text',
          value: 'pgTransactionId',
        },
        {
          header: 'Remarks',
          type: 'text',
          value: 'remarks',
        },
        {
          header: 'Transaction Status',
          type: 'text',
          value: 'transactionStatus',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        }
      ],
      
      api: {
        apiPath: '/transactions',
        method: 'GET',
        params : new HttpParams()
       
        .append('orderId', this.order.id)
        .append('transactionStatus',JSON.stringify({ne:'TXN_INITIATED'}))

      },
      countApi: {
        apiPath: '/transactions/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100, 200]
      },
      header: 'Transactions'
    }
  }

  ngOnDestroy(){
    if(this.subscription && !this.subscription.closed){
      this.subscription.unsubscribe();
    }
  }

}
