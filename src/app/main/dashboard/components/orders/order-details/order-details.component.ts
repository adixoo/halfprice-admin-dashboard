import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { BasicInformationComponent } from '../basic-information/basic-information.component';
import { DetailViewComponent } from '../detail-view/detail-view.component';
import { OrderPaymentsComponent } from '../order-payments/order-payments.component';
import { UpdateService } from '../updates/update.service';
import { UpdatesComponent } from '../updates/updates.component';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {

  public orderId:any;  
  public order:any;
  private subscription:Subscription;

  constructor(private activedRoute: ActivatedRoute,
       private apiService: CanApiService,
       private updateService : UpdateService) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.orderId = parseInt(paramMap.get('id')));
  }

  public tabViewConfig: CanTabView;
  public detailViewData: CanDetailView;


  async ngOnInit() {
    this.subscription =  this.updateService.isTransactionAdded$.subscribe(async (value) =>{
      // this.refreshEvent.next(value)
      if(value){
        await this.getOrder();
      }
    })
    await this.getOrder();

      this.tabViewConfig = {
        lazyLoading: true,
        loadsEveryTime:true,
        tabs: []
      };
      this.tabViewConfig.tabs.push({
        component: BasicInformationComponent,
        label: 'Basic Information ',
        inputData: [{
          key: 'order',
          value: this.order
        }]
      })
      // if (data.kycId) {
      
        this.tabViewConfig.tabs.push({
          component: DetailViewComponent,
          label: 'Order Details',
          inputData: [{
            key: 'order',
            value: this.order
          }]
        });
      // }
      this.tabViewConfig.tabs.push({
        component: UpdatesComponent,
        label: 'Updates',
        inputData: [{
          key: 'order',
          value: this.order
        }]
      });
      this.tabViewConfig.tabs.push({
        component: OrderPaymentsComponent,
        label: 'Payments',
        inputData: [{
          key: 'order',
          value: this.order
        }]
      });
    
  }
  getOrder(){
    return new Promise((resolve, reject) => {
      const getProperties: CanApi = {
        apiPath: `/orders/${this.orderId}`,
        method: "GET",
        params: new HttpParams()
        .append('include', JSON.stringify([{ all: true }]))
      };
      this.apiService.request(getProperties).subscribe((res: any[]) => {
        this.order = res;
        resolve(res);
      });
    });
  }

  // ngOnDestroy(){
  //   if(this.subscription && !this.subscription.closed){
  //     this.subscription.();
  //   }
  // }
}
