import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateBusinessComponent } from './create-business/create-business.component';
import { EditBusinessComponent } from './edit-business/edit-business.component';

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.scss']
})
export class BusinessesComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() {
    // Fetching Path Params
  }

  ngOnInit() {
    // Table Data Init
    this.tableConfig = {
      discriminator: "tableConfig",
      displayedColumns: [
        {
          header: "name",
          type: "text",
          value: "name",
        },
        {
          header:'Description',
          type:'text',
          value:'description'
        },
        {
          header: "Status",
          type: "enum_icon",
          value: "status",
          enumIcons: [
            {
              value: "active",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Active",
                color: "#10d817",
              },
            },
            {
              value: "inactive",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Inactive",
                color: "#ee3f37",
              },
            },
          ],
        },
      ],
      fieldActions: [
        {
          action: {
            actionType: 'link',

            link: {
              url: '/dashboard/supplier-business/${id}',
              target: 'self',
              type: 'url'
            }
            
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'Business',
          }

        },
        {
          action: {
            actionType: "modal",
            modal: {
              component: EditBusinessComponent,
              inputData: [
                {
                  inputKey: "businessId",
                  type: "key",
                  key: "id",
                },
              ],
              header: "Edit Business",
              width: 600,
            },
            
          },
          icon: {
            name: "edit",
            tooltip: "Edit Business",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/businesses/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "active",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to active business?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "inactive" },
            },
          },
          icon: {
            type: CanIconType.Material,
            name: "thumb_up_al",
            tooltip: "Change Status",
            color: "#10d817",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/businesses/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "inactive",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to inactive business?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "active" },
            },
          },
          icon: {
            // name: 'edit',
            // tooltip: 'change status',
            type: CanIconType.Material,
            name: "thumb_down_al",
            tooltip: "Change Status",
            color: "#ee3f37",
          },
        }
      ],
      api: {
        apiPath: "/businesses",
        method: "GET"
      },
      countApi: {
        apiPath: "/businesses/count",
        method: "GET"
      },
      countApiDataKey: "data.count",
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: "Businesses",
    };

    // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: "add",
        tooltip: "Create New",
      },
      type: "modal",
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateBusinessComponent,
        inputData: [],
        width: 600,
        header: "Add Business",
      },
     
    };
  }

}
