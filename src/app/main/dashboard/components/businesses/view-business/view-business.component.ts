import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { SuppliersComponent } from '../../suppliers/suppliers.component';
import { SpplierProductsComponent } from './spplier-products/spplier-products.component';

@Component({
  selector: 'app-view-business',
  templateUrl: './view-business.component.html',
  styleUrls: ['./view-business.component.scss']
})
export class ViewBusinessComponent implements OnInit {

  public detailViewData: CanDetailView;
  public tabViewConfig: CanTabView;

  @Input() businessId: any;
  dataSource : any
   
  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.businessId = parseInt(paramMap.get('id')));
  }
  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      // dataSource : this.dataSource,
      labelPosition: 'top',
      displayedFields: [
        {
          header: 'Name',
          type: 'text',
          value: 'name',
        },
        {
          header:'Description',
          type:'text',
          value:'description'
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        },
       
      ],
      api: {
        apiPath: `/businesses/${this.businessId}`,
        method: 'GET'
      },
    }
    this.tabViewConfig = {
      lazyLoading: true,
      loadsEveryTime:true,
      tabs: [{
        component: SuppliersComponent,
        label: 'Users',
        inputData: [{
          key: 'businessId',
          value: this.businessId
        }]
      },
      {
        component: SpplierProductsComponent,
        label: 'Products',
        inputData: [{
          key: 'businessId',
          value: this.businessId
        }]
      }
    ]
    };

  }

}
