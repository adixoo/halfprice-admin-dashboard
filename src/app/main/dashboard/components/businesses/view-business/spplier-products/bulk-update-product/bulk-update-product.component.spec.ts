import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUpdateProductComponent } from './bulk-update-product.component';

describe('BulkUpdateProductComponent', () => {
  let component: BulkUpdateProductComponent;
  let fixture: ComponentFixture<BulkUpdateProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkUpdateProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUpdateProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
