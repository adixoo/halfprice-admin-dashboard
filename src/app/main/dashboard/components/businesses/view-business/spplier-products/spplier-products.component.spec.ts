import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpplierProductsComponent } from './spplier-products.component';

describe('SpplierProductsComponent', () => {
  let component: SpplierProductsComponent;
  let fixture: ComponentFixture<SpplierProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpplierProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpplierProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
