import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { EditProductComponent } from '../../../products/edit-product/edit-product.component';
import { ProductDetailsComponent } from '../../../products/product-details/product-details.component';
import { BulkUpdateProductComponent } from './bulk-update-product/bulk-update-product.component';

@Component({
  selector: 'app-spplier-products',
  templateUrl: './spplier-products.component.html',
  styleUrls: ['./spplier-products.component.scss']
})
export class SpplierProductsComponent implements OnInit {

  @Input() businessId:any;

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() {
    // Fetching Path Params
  }

  ngOnInit() {
    // Table Data Init
    this.tableConfig = {
      discriminator: "tableConfig",
      displayedColumns: [
        {
          header: "name",
          type: "text",
          value: "name",
        },
        {
          header:'SKU',
          type : 'text',
          value: 'sku'
        },
        {
          header: 'Thumb Image',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'thumbImages'
              }
            ]
          }
        },
        {
          header: "Description",
          type: "text",
          value: "description",
        },
        {
          header:'Price',
          type:'text',
          value:'price'
        },
        {
          header: "Product Status",
          type: "text",
          value: "productStatus",
        },
       
        {
          header: "Brand Name",
          type: "text",
          value: "brandName",
        },
        {
          header: "Material Care",
          type: "text",
          value: "materialCare",
        },
      
        {
          header: "Available Stock",
          type: "text",
          value: "availableStock",
        },
        {
          header: "HSN Code",
          type: "text",
          value: "hsnCode",
        },
        {
          header: "Status",
          type: "enum_icon",
          value: "status",
          enumIcons: [
            {
              value: "active",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Active",
                color: "#10d817",
              },
            },
            {
              value: "inactive",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Inactive",
                color: "#ee3f37",
              },
            },
          ],
        },
      ],
      fieldActions: [
        {
          action: {
            actionType: "modal",
            modal: {
              component: EditProductComponent,
              inputData: [
                {
                  inputKey: "productId",
                  type: "key",
                  key: "id",
                },
              ],
              header: "Edit Product",
              width: 600,
            },
            
          },
          icon: {
            name: "edit",
            tooltip: "Edit Product",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/products/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "productStatus",
                value: "approved",
              },
            ],
            confirm: {
              title: "Approved product",
              message: "Are you sure you want to approved product?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
      
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "productStatus", value: "pending" },
            },
          },
          icon: {
            type: CanIconType.Material,
            name: "thumb_up_al",
            tooltip: "Approved product",
            color: "#10d817",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/products/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "productStatus",
                value: "rejected",
              },
            ],
            confirm: {
              title: "Rejected product",
              message: "Are you sure you want to rejected product?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
              displayCondition: {
              type: "single",
              match: { operator: "equals", key: "productStatus", value: "pending" },
            },
          },
          icon: {
            // name: 'edit',
            // tooltip: 'change status',
            type: CanIconType.Material,
            name: "thumb_down_al",
            tooltip: "Rejected product",
            color: "#ee3f37",
          },
        },
        {
          action: {
            actionType: "modal",
            modal: {
              component: ProductDetailsComponent,
              inputData: [],
              // header: 'Product Details',
              width: 600,
            },
          },
          icon: {
            type: CanIconType.Flaticon,
            name: "flaticon-eye",
            tooltip: "View Product",
          },
        },
           
      ],
      filters: [
      
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['name'],
            api: {
              apiPath: '/products',
              method: 'GET',
              params : new HttpParams()
            }
          },
        },
        {
          filtertype: "api",
          placeholder: "Status",
          type: "dropdown",
          key: "status",
          value: [
            { value: "active", viewValue: "Active" },
            { value: "inactive", viewValue: "Inactive" },
          ],
        },
        {
          filtertype: "api",
          placeholder: "Product Status",
          type: "dropdown",
          key: "productStatus",
          value: [
            { value: "pending", viewValue: "pending" },
            { value: "approved", viewValue: "approved" },
            { value: "rejected", viewValue: "rejected" },

          ],
        },
      ],
      api: {
        apiPath: "/products",
        method: "GET",
        params: new HttpParams().append(
          "include",
          JSON.stringify([{ all: true }])
        )
        .append('businessId',this.businessId)
        .append('order', JSON.stringify([['updatedAt', 'DESC']]))
      },
      countApi: {
        apiPath: "/products/count",
        method: "GET",
        params: new HttpParams().append(
          "include",
          JSON.stringify([{ all: true }])
        ),
      },
      
      countApiDataKey: "count",
      pagination: {
        pageSizeOptions: [50, 100]
      },
      // header: "Products",
    };

    // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: "add",
        tooltip: "Create New",
      },
      type: "modal",
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: BulkUpdateProductComponent,
        inputData: [],
        width: 600,
        header: "Bulk Update Products",
      },
     
    };
  }

}
