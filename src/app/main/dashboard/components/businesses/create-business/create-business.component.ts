import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CanForm } from 'src/@can/types/form.type';
import { BusinessAddressComponent } from '../business-address/business-address.component';

@Component({
  selector: 'app-create-business',
  templateUrl: './create-business.component.html',
  styleUrls: ['./create-business.component.scss']
})
export class CreateBusinessComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  constructor(  public dialog: MatDialog) { }

  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          required: {
            value: true,
            errorMessage: 'Description is required.'
          }
        },
            
        // {
        //   name: 'banners',
        //   type: 'image',
        //   placeholder: 'Upload Banner Image',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 2,
        //     multipleSelect: true,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      submitApi: {
        apiPath: `/businesses`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Business created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

  getData(data){
    const dialogRef = this.dialog.open(BusinessAddressComponent,
      {
        width: '600px',
        data: { businessId : data['id'] }
      }
    );
    dialogRef.afterClosed().subscribe(submitResponse => {
    });
  }

}
