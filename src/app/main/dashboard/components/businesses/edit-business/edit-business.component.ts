import { EventEmitter, Input, Output } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-business',
  templateUrl: './edit-business.component.html',
  styleUrls: ['./edit-business.component.scss']
})
export class EditBusinessComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() businessId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },      
        // {
        //   name: 'banners',
        //   type: 'image',
        //   placeholder: 'Upload Banner Image',
        //   value:'banners',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'image/*',
        //     limit: 2,
        //     multipleSelect: true,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          value:'description',
          required: {
            value: true,
            errorMessage: 'Description is required.'
          }
        },
      
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/businesses/${this.businessId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/businesses/${this.businessId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Business updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
