import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-business-address',
  templateUrl: './business-address.component.html',
  styleUrls: ['./business-address.component.scss']
})
export class BusinessAddressComponent implements OnInit {
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;
  businessId:any;

  constructor( 
    public dialogRef: MatDialogRef<BusinessAddressComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
   ) {
     this.businessId = data.businessId
    }

  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'address',
          type: 'text',
          placeholder: 'Address',
          required: {
            value: true,
            errorMessage: 'Address is required.'
          }
        },
        {
          name: 'pinCode',
          type: 'text',
          placeholder: 'Pin Code',
          required: {
            value: true,
            errorMessage: 'Pin Code is required.'
          }
        },
        {
          name: 'panNumber',
          type: 'text',
          placeholder: 'Pan Number'
        },
        {
          name: 'country',
          type: 'text',
          placeholder: 'Country',
          defaultValue:'India',
          disabled:true
        },  

        {
          name: 'State',
          type: 'text',
          placeholder: 'State',
        },  

        {
          name: 'city',
          type: 'text',
          placeholder: 'City',
        },  
        {
          name:'businessId',
          type:'hidden',
          placeholder:null,
          defaultValue:this.businessId
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      submitApi: {
        apiPath: `/addresses`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Business addresses created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

  closeModal(){
    this.dialogRef.close();
  }

}
