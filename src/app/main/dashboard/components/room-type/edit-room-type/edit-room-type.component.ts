import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-room-type',
  templateUrl: './edit-room-type.component.html',
  styleUrls: ['./edit-room-type.component.scss']
})
export class EditRoomTypeComponent implements OnInit {

 
  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() roomTypeId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'image',
          type: 'image',
          placeholder: 'Upload Image',
          value:'image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'icon',
          type: 'image',
          placeholder: 'Upload Icon',
          value:'icon',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
      ],
      getApi:{
        apiPath: `/room-types/${this.roomTypeId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/room-types/${this.roomTypeId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Room Type updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
