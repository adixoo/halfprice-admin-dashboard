import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-package',
  templateUrl: './edit-package.component.html',
  styleUrls: ['./edit-package.component.scss']
})
export class EditPackageComponent implements OnInit {

  
  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() packageId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'type',
          type: 'text',
          placeholder: 'Type',
          value:'type',
          required: {
            value: true,
            errorMessage: 'Type is required.'
          }
        },
        {
          name: 'priority',
          type: 'number',
          placeholder: 'Priority',
          value:'priority',
          required: {
            value: true,
            errorMessage: 'Priority required.'
          }
        },
        {
          name: 'icon',
          type: 'image',
          placeholder: 'Upload ICON Image',
          value:'icon',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        // {
        //   name: "towerId",
        //   type: "autocomplete",
        //   placeholder: "Tower",
        //   value:'towerId',
        //   required: {
        //     value: true,
        //     errorMessage: "Tower is required."
        //   },
        //   autoComplete: {
        //     type: "api",
        //     api: {
        //       apiPath: "/towers",
        //       method: "GET",
        //       // params: new HttpParams().append("isInternal", "true")
        //     },
        //     autocompleteParamKeys: ["name"],
        //     apiValueKey: "id",
        //     apiViewValueKey: "name"
        //   }
        // },
        {
          name: "builderId",
          type: "autocomplete",
          placeholder: "Builder",
          value:'builderId',
          required: {
            value: true,
            errorMessage: "Builder is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/builders",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: "projectId",
          type: "autocomplete",
          placeholder: "Project",
          value:'projectId',
          required: {
            value: true,
            errorMessage: "Project is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/projects",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        // {
        //   name: 'packageDetails',
        //   type: 'array',
        //   placeholder: 'Package Details',
        //   value:'packageDetails',
        //   formArray: {
        //     addButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Add Package'
        //     },
        //     deleteButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Delete Package'
        //     }
        //   },
          
        //   formFields: [    
          
        //     {
        //       name: 'type',
        //       type: 'select',
        //       placeholder: 'Type',
        //       select: {
        //         type: 'api',
        //         api: {
        //           apiPath: '/room-types',
        //           method: 'GET'
        //         },
        //         apiValueKey: "name",
        //         apiViewValueKey: "name"
        //       },
        //       required: {
        //         value: true,
        //         errorMessage: 'Type is required!'
        //       }
        //     },
        //     {
        //       name: 'categories',
        //       type: 'array',
        //       placeholder: 'Categories',
        //       formArray: {
        //         addButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Add Category'
        //         },
        //         deleteButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Delete Category'
        //         }
        //       },
        //       formFields: [   
        //         {
        //           name: 'name',
        //           type: 'select',
        //           placeholder: 'Names',
        //           select: {
        //             type: 'api',
        //             api: {
        //               apiPath: '/categories',
        //               method: 'GET'
        //             },
        //             apiValueKey: "name",
        //             apiViewValueKey: "name"
        //           },
        //           required: {
        //             value: true,
        //             errorMessage: 'Name is required!'
        //           },
        //           relativeSetFields:[{
        //             key:'id',
        //             type:'different',
        //             value :'id',
        //             differentType:'dataSource'
        //           }]
              
        //         }, 
        //         {
        //           name:'id',
        //           type:'hidden',
        //           value:'id',
        //           placeholder:null
        //         },
        //         {
        //           name: 'items',
        //           type: 'array',
        //           placeholder: 'items',
        //           formArray: {
        //             addButton: {
        //               type: 'raised',
        //               color: 'primary',
        //               label: 'Add Items'
        //             },
        //             deleteButton: {
        //               type: 'raised',
        //               color: 'primary',
        //               label: 'Delete Items'
        //             }
        //           },
        //           formFields: [   
        //             {
        //               name: 'brand',
        //               type: 'select',
        //               placeholder: 'Brand',
        //               select: {
        //                 type: 'api',
        //                 api: {
        //                   apiPath: '/products',
        //                   method: 'GET'
        //                 },
        //                 apiValueKey: "brandName",
        //                 apiViewValueKey: "name"
        //               },
        //               required: {
        //                 value: true,
        //                 errorMessage: 'Brand is required!'
        //               },
        //               relativeSetFields:[{
        //                 key:'pId',
        //                 type:'different',
        //                 value :'id',
        //                 differentType:'dataSource'
        //               },
        //               {
        //                 key:'price',
        //                 type:'different',
        //                 value :'sellingPrice',
        //                 differentType:'dataSource'
        //               }]
                  
        //             }, 
        //             {
        //               name:'pId',
        //               type:'hidden',
        //               value:'id',
        //               placeholder:null,
        //               relativeSetFields:[{
        //                 key:'price',
        //                 type:'different',
        //                 value :'price',
        //                 differentType:'dataSource'
        //               }]
        //             },
        //             {
        //               name:'price',
        //               type:'hidden',
        //               value:'price',
        //               placeholder:null,
        //               // required: {
        //               //   value: true,
        //               //   errorMessage: "Price is required."
        //               // },
        //             }
        //           ]
        //         }
        //       ]
        //     }
        //   ]
        // },
      ],
      getApi:{
        apiPath: `/packages/${this.packageId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/packages/${this.packageId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Packages updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }


}
