import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreatePackageTypeComponent } from './create-package-type/create-package-type.component';
import { EditPackageTypeComponent } from './edit-package-type/edit-package-type.component';

@Component({
  selector: 'app-package-type',
  templateUrl: './package-type.component.html',
  styleUrls: ['./package-type.component.scss']
})
export class PackageTypeComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() { }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [
          {
            header: 'Name',
            type: 'text',
            value: 'name',
          },
          {
            header: 'Image',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'image'
                }
              ]
            }
          },
          {
            header: 'Status',
            type: 'text',
            value: 'status',
          }
        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditPackageTypeComponent,
                inputData: [
                  {
                    inputKey: 'packageTypeId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Package Type',
                width: 600
              }
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Package Type',
            }
          }
        ],
        filters: [
          {
            filtertype: 'api',
            placeholder: 'Search',
            type: 'text',
            key: 'id',
            searchType: 'autocomplete',
            autoComplete: {
              type: 'api',
              apiValueKey: 'id',
              apiViewValueKey: 'name',
              autocompleteParamKeys: ['name'],
              api: {
                apiPath: '/package-types',
                method: 'GET'
              }
            },
          },
        ],
        api: {
          apiPath: '/package-types',
          method: 'GET'
        },
        countApi: {
          apiPath: '/package-types/count',
          method: 'GET'
  
        },
        countApiDataKey: 'count',
        pagination: {
          pageSizeOptions: [100, 200]
        },
        header: 'Package Types'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreatePackageTypeComponent,
        inputData: [],
        width: 600,
        header: 'Add Package Type'
      },
    }
  }
}
