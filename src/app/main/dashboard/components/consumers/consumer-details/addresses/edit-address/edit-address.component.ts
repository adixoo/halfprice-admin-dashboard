import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.scss']
})
export class EditAddressComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() addressId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [        
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'registeredName',
          type: 'text',
          placeholder: 'Register Name',
          value:'registeredName'
        },
        {
          name: 'gstin',
          type: 'text',
          placeholder: 'GSTIN',
          value:'gstin'
        },
        // {
        //   name: 'gstinImage',
        //   type: 'group',
        //   placeholder: '',
        //   value: 'gstinImage',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'path',
        //     acceptType: 'image/*',
        //     multipleSelect: true
        //   },
        //   suffixIcon: { name: 'attach_file' },
        //   // formFields: [
        //   //   {
        //   //     name: 'path',
        //   //     type: 'image',
        //   //     placeholder: 'Select Images',
        //   //     file: {
        //   //       api: {
        //   //         apiPath: '/files/upload',
        //   //         method: 'POST'
        //   //       },
        //   //       apiKey: 'path',
        //   //       acceptType: 'image/*',
        //   //       multipleSelect: true
        //   //     },
        //   //     suffixIcon: { name: 'attach_file' },
        //   //   }]
        // },
        {
          name: 'panNumber',
          type: 'text',
          placeholder: 'Pan Number',
          value:'panNumber'
        },
        {
          name: 'country',
          type: 'text',
          placeholder: 'Country',
          value:'country'
        },
        {
          name: 'state',
          type: 'text',
          placeholder: 'State',
          value:'state',
        },
        {
          name: 'city',
          type: 'text',
          placeholder: 'City',
          value:'city',
        },  
          {
          name: 'pinCode',
          type: 'text',
          placeholder: 'Pin Code',
          value:'pinCode',
        },
        {
          name: 'address',
          type: 'text',
          placeholder: 'Address',
          value:'address',
        }, 
      ],
      getApi:{
        apiPath: `/addresses/${this.addressId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/addresses/${this.addressId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Address updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
