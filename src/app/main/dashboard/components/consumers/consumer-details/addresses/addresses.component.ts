import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { EditConsumerComponent } from '../../edit-consumer/edit-consumer.component';
import { EditAddressComponent } from './edit-address/edit-address.component';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.scss']
})
export class AddressesComponent implements OnInit {

  constructor() { }
  @Input() userId :any;
  public tableConfig: CanTable;

  ngOnInit() {
 
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'name'
        },
        {
          header: 'Register Name',
          type: 'text',
          value: 'registeredName',
        },
        {
          header:'GSTIN',
          type:'text',
          value:'gstin'
        },
        {
          header: 'GSTIN Image',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'gstinImage'
              }
            ]
          }
        },
        {
          header: 'Pan Number',
          type: 'text',
          value: 'panNumber',
        },
        {
          header: 'Country',
          type: 'text',
          value: 'country',
        },
        {
          header: 'State',
          type: 'text',
          value: 'state',
        },
        {
          header: 'City',                                                                                                    
          type: 'text',
          value: 'city',
        },
        {
          header: 'Pin Code',
          type: 'text',
          value: 'pinCode',
        },
        {
          header: 'Address',
          type: 'text',
          value: 'address',
        },
        {
          header: 'Type',
          type: 'text',
          value: 'type',
        }
      ],
      // filters: [
      //   {
      //     filtertype: 'local',
      //     placeholder: 'Search',
      //     keys: ['username', 'name', 'email', 'mobile']
      //   }
      // ],
      fieldActions: [
  
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditAddressComponent,
              inputData: [
                {
                  inputKey: 'addressId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit Adderess',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit Address',
          }
        },
       
        // {
        //   action: {
        //     actionType: 'link',
        //     link: {
        //       url: '/dashboard/consumer/${id}',
        //       target: 'self',
        //       type: 'url'
        //     }
        //   },
        //   icon: {
        //     type: CanIconType.Flaticon,
        //     name: 'flaticon-eye',
        //     tooltip: 'View'
        //   }
        // },
        // {
        //   action: {
        //     actionType: 'link',

        //     link: {
        //       url: '/dashboard/sub-category/${id}',
        //       target: 'self',
        //       type: 'url'
        //     }
        //     // modal: {
        //     //   component: ServiceSubTableComponent,
        //     //   inputData: [
        //     //     {
        //     //       inputKey: 'serviceId',
        //     //       type: 'key',
        //     //       key: 'id'
        //     //     }
        //     //   ],
        //     //   header: 'Service Sub Categories',
        //     //   width: 1000
        //     // },

        //   },
        //   icon: {
        //     type: CanIconType.Flaticon,
        //     name: 'flaticon-eye',
        //     tooltip: 'Service Sub Categories',
        //   }

        // }
      ],
      api: {
        apiPath: '/addresses',
        method: 'GET',
        params : new HttpParams().set('userId',JSON.stringify(this.userId))
      },
      // countApi: {
      //   apiPath: '/addresses/count',
      //   method: 'GET',
      //   // params : new HttpParams().set('userId',JSON.stringify(this.userId))


      // },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 , 200]
      },
      header: 'Addresses'
    }
  }
}
