import { HttpParams } from "@angular/common/http";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { CanForm } from "src/@can/types/form.type";

@Component({
  selector: "app-create-product",
  templateUrl: "./create-product.component.html",
  styleUrls: ["./create-product.component.scss"],
})
export class CreateProductComponent implements OnInit {
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      columnPerRow: 1,
      sendAll: true,
      formFields: [
        {
          name: "name",
          type: "text",
          placeholder: "Name",
          required: {
            value: true,
            errorMessage: "Name is required.",
          },
        },
        {
          name: "description",
          type: "textarea",
          placeholder: "Description",
          required: {
            value: true,
            errorMessage: "Description is required.",
          },
        },
        {
          name: "sku",
          type: "text",
          placeholder: "SKU",
          required: {
            value: true,
            errorMessage: "SKU is required.",
          },
        },

        {
          name: "price",
          type: "text",
          placeholder: "Price",
          required: {
            value: true,
            errorMessage: "Price is required.",
          },
        },
        {
          name: "sellingPrice",
          type: "text",
          placeholder: "Selling Price",
          required: {
            value: true,
            errorMessage: "Selling Price is required.",
          },
        },
        {
          name: "hsnCode",
          type: "text",
          placeholder: "HSN Code",
        },
        {
          name: "materialCare",
          type: "text",
          placeholder: "Material Care",
        },
        {
          name: "brandName",
          type: "text",
          placeholder: "Brand Name",
          required: {
            value: true,
            errorMessage: "Brand Name is required.",
          },
        },
        {
          name: "status",
          type: "select",
          placeholder: "Status",
          values: [
            { value: "active", viewValue: "active" },
            { value: "inactive", viewValue: "inactive" },
          ],
          required: {
            value: true,
            errorMessage: "Status is required.",
          },
        },
        {
          name: "thumbImages",
          type: "image",
          placeholder: "Upload Thumb Images",
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
            },
            apiKey: "files.paths",
            payloadName: "files",
            acceptType: "image/*",
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: "attach_file" },
        },

        {
          name: "images",
          type: "image",
          placeholder: "Upload  Images",
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
            },
            apiKey: "files[0].path",
            payloadName: "files",
            acceptType: "image/*",
            limit: 5,
            multipleSelect: true,
          },
          suffixIcon: { name: "attach_file" },
        },
        // {
        //   name: 'categoryId',
        //   type: 'select',
        //   placeholder: 'category',
        //   select: {
        //     type: 'relative',
        //     api: {
        //       apiPath: '/categories',
        //       method: 'GET',
        //     },
        //     apiViewValueKey: 'name',
        //     apiValueKey: 'id'
        //   },
        //   relatedTo: ['subCategoryId'],
        //   send: 'notSend'
        // },
        {
          name: "categoryId",
          type: "autocomplete",
          send:'notSend',
          placeholder: "Category",
          required: {
            value: true,
            errorMessage: "Category is required."
          },
          relatedTo:['subCategoryId'],
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/categories",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'subCategoryId',
          type: 'select',
          placeholder: 'Sub Category',
          select: {
            type: 'api',
            api: {
              apiPath: '/sub-categories',
              method: 'GET',
            },
            apiViewValueKey: 'name',
            apiValueKey: 'id',
            relativeApiInfo: {
              queryParams: [{ key: 'categoryId', value: 'categoryId' }]
            }
          }
        },
        
        // {
        //   name: "subCategoryId",
        //   type: "autocomplete",
        //   placeholder: "sub Category",
        //   required: {
        //     value: true,
        //     errorMessage: "Sub categories is required."
        //   },
        //   autoComplete: {
        //     type: "api",
        //     api: {
        //       apiPath: "/sub-categories",
        //       method: "GET",
        //       params: new HttpParams().append("categoryId", "$categoryId")
        //     },
            
        //     autocompleteParamKeys: ["name"],
        //     apiValueKey: "id",
        //     apiViewValueKey: "name"
            
        //   },


        // },
        // {
        //   name: 'userManual',
        //   type: 'document',
        //   placeholder: 'User Manual',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'data.files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'csv/*, pdf/*',
        //     limit: 1,
        //     multipleSelect: false,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        {
          name: "availableStock",
          type: "number",
          placeholder: "Available Stock",
          required: {
            value: true,
            errorMessage: "Available Stock is required.",
          },
        },
        {
          name:'productStatus',
          type:"hidden",
          placeholder:null
        },
        {
          name: "specifications",
          type: "array",
          placeholder: "Additional Info",
          columnPerRow: 2,
          formArray: {
            addButton: {
              type: "raised",
              color: "primary",
              label: "+",
            },
            deleteButton: {
              type: "raised",
              color: "primary",
              label: "-",
            },
          },
          formFields: [
            {
              name: "key",
              type: "text",
              placeholder: "Key",
            },
            {
              name: "value",
              type: "text",
              placeholder: "Value",
            },
          ],
        },
      ],
      submitApi: {
        apiPath: `/products`,
        method: "POST",
      },
      formButton: {
        type: "raised",
        color: "primary",
        label: "Save",
      },
      submitSuccessMessage: "Product created successfully!",
    };
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
