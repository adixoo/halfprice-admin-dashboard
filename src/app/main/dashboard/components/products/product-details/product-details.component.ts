import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanIconType } from 'src/@can/types/shared.type';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  
  public detailViewData: CanDetailView;
  dataSource : any

  constructor() { }

  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 1,
      dataSource:this.dataSource,
      header:this.dataSource.name,
      labelPosition: 'inline',
      displayedFields:  [
        {
          header: "name",
          type: "text",
          value: "name",
        },
        {
          header:'SKU',
          type : 'text',
          value: 'sku'
        },
        {
          header: 'Thumb Image',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'thumbImages'
              }
            ]
          }
        },
        {
          header: "Description",
          type: "text",
          value: "description",
        },
        {
          header:'Price',
          type:'text',
          value:'price'
        },
        {
          header: "Product Status",
          type: "text",
          value: "productStatus",
        },
       
        {
          header: "Brand Name",
          type: "text",
          value: "brandName",
        },
        {
          header: "Material Care",
          type: "text",
          value: "materialCare",
        },
      
        {
          header: "Available Stock",
          type: "text",
          value: "availableStock",
        },
        {
          header: "HSN Code",
          type: "text",
          value: "hsnCode",
        },
        {
          header: "Status",
          type: "enum_icon",
          value: "status",
          enumIcons: [
            {
              value: "active",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Active",
                color: "#10d817",
              },
            },
            {
              value: "inactive",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Inactive",
                color: "#ee3f37",
              },
            },
          ],
        },
      ],
 
      // header: "Master Products Details"
    }
  }
}
