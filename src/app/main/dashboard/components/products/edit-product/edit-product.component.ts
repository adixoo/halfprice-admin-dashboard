import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() productId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: "name",
          type: "text",
          value: "name",
          placeholder: "Name",
          required: {
            value: true,
            errorMessage: "Name is required.",
          },
        },
        {
          name: "description",
          value: "description",
          type: "textarea",
          placeholder: "Description",
          required: {
            value: true,
            errorMessage: "Description is required.",
          },
        },
        {
          name: "sku",
          value: "sku",
          type: "text",
          placeholder: "SKU",
          required: {
            value: true,
            errorMessage: "SKU is required.",
          },
        },

        {
          name: "price",
          value: "price",
          type: "text",
          placeholder: "Price",
          required: {
            value: true,
            errorMessage: "Price is required.",
          },
        },
        {
          name: "sellingPrice",
          value: "sellingPrice",
          type: "text",
          placeholder: "Selling Price",
          required: {
            value: true,
            errorMessage: "Selling Price is required.",
          },
        },
        {
          name: "hsnCode",
          value: "hsnCode",
          type: "text",
          placeholder: "HSN Code",
        },
        {
          name: "materialCare",
          value: "materialCare",
          type: "text",
          placeholder: "Material Care",
        },
        {
          name: "brandName",
          value:"brandName",
          type: "text",
          placeholder: "Brand Name",
          required: {
            value: true,
            errorMessage: "Brand Name is required.",
          },
        },
        {
          name: "status",
          value: "status",
          type: "select",
          placeholder: "Status",
          values: [
            { value: "active", viewValue: "active" },
            { value: "inactive", viewValue: "inactive" },
          ],
          required: {
            value: true,
            errorMessage: "Status is required.",
          },
        },
        {
          name: "thumbImages",
          value: "thumbImages",
          type: "image",
          placeholder: "Upload Thumb Images",
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
            },
            apiKey: "files.paths",
            payloadName: "files",
            acceptType: "image/*",
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: "attach_file" },
        },

        {
          name: "images",
          value: "images",
          type: "image",
          placeholder: "Upload  Images",
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST",
            },
            apiKey: "files[0].path",
            payloadName: "files",
            acceptType: "image/*",
            limit: 5,
            multipleSelect: true,
          },
          suffixIcon: { name: "attach_file" },
        },
        // {
        //   name: 'categoryId',
        //   type: 'select',
        //   placeholder: 'category',
        //   select: {
        //     type: 'relative',
        //     api: {
        //       apiPath: '/categories',
        //       method: 'GET',
        //     },
        //     apiViewValueKey: 'name',
        //     apiValueKey: 'id'
        //   },
        //   relatedTo: ['subCategoryId'],
        //   send: 'notSend'
        // },
        {
          name: "categoryId",
          value: "categoryId",
          type: "autocomplete",
          send:'notSend',
          placeholder: "Category",
          required: {
            value: true,
            errorMessage: "Category is required."
          },
          relatedTo:['subCategoryId'],
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/categories",
              method: "GET"
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'subCategoryId',
          value: 'subCategoryId',
          type: 'select',
          placeholder: 'Sub Category',
          select: {
            type: 'api',
            api: {
              apiPath: '/sub-categories',
              method: 'GET',
            },
            apiViewValueKey: 'name',
            apiValueKey: 'id',
            relativeApiInfo: {
              queryParams: [{ key: 'categoryId', value: 'categoryId' }]
            }
          }
        },
        
        // {
        //   name: "subCategoryId",
        //   type: "autocomplete",
        //   placeholder: "sub Category",
        //   required: {
        //     value: true,
        //     errorMessage: "Sub categories is required."
        //   },
        //   autoComplete: {
        //     type: "api",
        //     api: {
        //       apiPath: "/sub-categories",
        //       method: "GET",
        //       params: new HttpParams().append("categoryId", "$categoryId")
        //     },
            
        //     autocompleteParamKeys: ["name"],
        //     apiValueKey: "id",
        //     apiViewValueKey: "name"
            
        //   },


        // },
        // {
        //   name: 'userManual',
        //   type: 'document',
        //   placeholder: 'User Manual',
        //   file: {
        //     api: {
        //       apiPath: '/files/upload',
        //       method: 'POST'
        //     },
        //     apiKey: 'data.files[0].path',
        //     payloadName: 'files',
        //     acceptType: 'csv/*, pdf/*',
        //     limit: 1,
        //     multipleSelect: false,
        //   },
        //   suffixIcon: { name: 'attach_file' },
        // },
        {
          name: "availableStock",
          value: "availableStock",
          type: "number",
          placeholder: "Available Stock",
          required: {
            value: true,
            errorMessage: "Available Stock is required.",
          },
        },
        {
          name:'productStatus',
          value:'productStatus',
          type:"hidden",
          placeholder:null
        },
        {
          name: "specifications",
          value: "specifications",
          type: "array",
          placeholder: "Additional Info",
          columnPerRow: 2,
          formArray: {
            addButton: {
              type: "raised",
              color: "primary",
              label: "+",
            },
            deleteButton: {
              type: "raised",
              color: "primary",
              label: "-",
            },
          },
          formFields: [
            {
              name: "key",
              type: "text",
              placeholder: "Key",
            },
            {
              name: "value",
              type: "text",
              placeholder: "Value",
            },
          ],
        },
      ],
      getApi:{
        apiPath: `/products/${this.productId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/products/${this.productId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Products updated successfully!"
    }
  }


  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
