import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-active-users-list',
  templateUrl: './active-users-list.component.html',
  styleUrls: ['./active-users-list.component.scss']
})
export class ActiveUsersListComponent implements OnInit {

  @Input() productId:any;

  public tableData: CanTable;
  constructor(
    private canApiService : CanApiService
  ) { }

  ngOnInit() {
    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Mobile',
          type: 'text',
          value: 'user.mobile'
        },
        {
          header: 'Device Type',
          type: 'text',
          value: 'product.type'
        },
        {
          header:'Device Version',
          type:'text',
          value:'device.appVersion'
        },
  
        {
          header:'Mac Address',
          type:'text',
          value:'wearable.macAddr'
        },
        {
          header:'Model Number',
          type:'text',
          value:'wearable.modelNo'
        },
        {
          header:'Last used time',
          type:'date',
          value :'updatedAt',
          dateDisplayType: 'dd/MM/yyyy'
        }
      ],
      api: {
        apiPath: '/user-products',
        method: 'GET',
        params : new HttpParams()
        .append('productId',this.productId)
        .append('status','active')
        .append('include', JSON.stringify([{  all: true  }]))
      },
      countApi:{
        apiPath:'/user-products/count',
        method:'GET',
       
      },
      apiDataKey: "data",
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Active Users',
    }
  
  }

}
