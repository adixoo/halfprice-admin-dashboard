import { HttpParams } from "@angular/common/http";
import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ApiType } from "src/@can/types/api.type";
import {
  CanFabButton,
  CanFabButtonPosition,
} from "src/@can/types/fab-button.type";
import { CanIconType } from "src/@can/types/shared.type";
import { CanTable } from "src/@can/types/table.type";
import { ActiveUsersListComponent } from "./active-users-list/active-users-list.component";
import { CreateProductComponent } from "./create-product/create-product.component";
import { EditProductComponent } from "./edit-product/edit-product.component";
import { ProductDetailsComponent } from "./product-details/product-details.component";
import { ProductFAQComponent } from "./product-faq/product-faq.component";

@Component({
  selector: "app-products",
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"],
})
export class ProductsComponent implements OnInit {
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;

  constructor() {
    // Fetching Path Params
  }

  ngOnInit() {
    // Table Data Init
    this.tableConfig = {
      discriminator: "tableConfig",
      displayedColumns: [
        {
          header: "name",
          type: "text",
          value: "name",
        },
        {
          header:'SKU',
          type : 'text',
          value: 'sku'
        },
        {
          header: 'Thumb Image',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'thumbImages'
              }
            ]
          }
        },
        {
          header: "Description",
          type: "text",
          value: "description",
        },
        {
          header:'Price',
          type:'text',
          value:'price'
        },
        {
          header: "Product Status",
          type: "text",
          value: "productStatus",
        },
       
        {
          header: "Brand Name",
          type: "text",
          value: "brandName",
        },
        {
          header: "Material Care",
          type: "text",
          value: "materialCare",
        },
      
        {
          header: "Available Stock",
          type: "text",
          value: "availableStock",
        },
        {
          header: "HSN Code",
          type: "text",
          value: "hsnCode",
        },
        {
          header: "Status",
          type: "enum_icon",
          value: "status",
          enumIcons: [
            {
              value: "active",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Active",
                color: "#10d817",
              },
            },
            {
              value: "inactive",
              icon: {
                type: CanIconType.Material,
                name: "fiber_manual_record",
                tooltip: "Inactive",
                color: "#ee3f37",
              },
            },
          ],
        },
      ],
      fieldActions: [
        {
          action: {
            actionType: "modal",
            modal: {
              component: EditProductComponent,
              inputData: [
                {
                  inputKey: "productId",
                  type: "key",
                  key: "id",
                },
              ],
              header: "Edit Product",
              width: 600,
            },
            
          },
          icon: {
            name: "edit",
            tooltip: "Edit Product",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/products/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "active",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to active product?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
      
            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "inactive" },
            },
          },
          icon: {
            type: CanIconType.Material,
            name: "thumb_up_al",
            tooltip: "Change Status",
            color: "#10d817",
          },
        },
        {
          action: {
            actionType: "ajax",
            api: {
              apiPath: "/products/${id}",
              method: "PATCH",
            },
            bodyParams: [
              {
                key: "status",
                value: "inactive",
              },
            ],
            confirm: {
              title: "Change Status",
              message: "Are you sure you want to inactive product?",
              buttonText: {
                confirm: "Confirm",
                cancel: "Cancel",
              },
            },
          permission: { type: 'single', match: { key: 'UPDATE_PRODUCTS', value: true } },

            displayCondition: {
              type: "single",
              match: { operator: "equals", key: "status", value: "active" },
            },
          },
          icon: {
            // name: 'edit',
            // tooltip: 'change status',
            type: CanIconType.Material,
            name: "thumb_down_al",
            tooltip: "Change Status",
            color: "#ee3f37",
          },
        },
        {
          action: {
            actionType: "modal",
            modal: {
              component: ProductDetailsComponent,
              inputData: [],
              // header: 'Product Details',
              width: 600,
            },
          },
          icon: {
            type: CanIconType.Flaticon,
            name: "flaticon-eye",
            tooltip: "View Product",
          },
        },
     
        // {
        //   action: {
        //     actionType: "modal",
        //     modal: {
        //       component: ProductFAQComponent,
        //       inputData: [
        //         {
        //           inputKey: "productId",
        //           type: "key",
        //           key: "id",
        //         }
        //       ],
        //       width: 800,
        //     },
        //   },
        //   icon: {
        //     type: CanIconType.Flaticon,
        //     name: "flaticon-eye",
        //     tooltip: "View FAQ",
        //   },
        // },
      
      ],
      filters: [
      
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['name'],
            api: {
              apiPath: '/products',
              method: 'GET',
              params : new HttpParams()
            }
          },
        },
        {
          filtertype: "api",
          placeholder: "Status",
          type: "dropdown",
          key: "status",
          value: [
            { value: "active", viewValue: "Active" },
            { value: "inactive", viewValue: "Inactive" },
          ],
        },
      ],
      api: {
        apiPath: "/products",
        method: "GET",
        params: new HttpParams().append(
          "include",
          JSON.stringify([{ all: true }])
        ),
      },
      countApi: {
        apiPath: "/products/count",
        method: "GET",
        params: new HttpParams().append(
          "include",
          JSON.stringify([{ all: true }])
        ),
      },
      
      countApiDataKey: "count",
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: "Products",
    };

    // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: "add",
        tooltip: "Create New",
      },
      type: "modal",
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateProductComponent,
        inputData: [],
        width: 600,
        header: "Add product",
      },
     
    };
  }
}
