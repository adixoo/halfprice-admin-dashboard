import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanFabButton } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { DetailFaqComponent } from '../../faq/detail-faq/detail-faq.component';
import { EditFaqComponent } from '../../faq/edit-faq/edit-faq.component';

@Component({
  selector: 'app-product-faq',
  templateUrl: './product-faq.component.html',
  styleUrls: ['./product-faq.component.scss']
})
export class ProductFAQComponent implements OnInit {

  @Input() productId:any;

  public tableData: CanTable;
  public fabButtonConfig: CanFabButton;
  constructor() { }

  ngOnInit() {
    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Title',
          type: 'text',
          value: 'title'
        },
        {
          header: 'Created At',
          type: 'date',
          value: 'createdAt',
          dateDisplayType: 'dd/MM/yyyy'
        },
        {
          header:'Updated At',
          type:'date',
          value :'updatedAt',
          dateDisplayType: 'dd/MM/yyyy'
        },
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'status',
          enumIcons: [
            { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
            { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

          ]
        },
      ],

      fieldActions: [
        {
          action: {
            actionType: 'ajax',
            api: {
              apiPath: '/faqs/${id}',
              method: 'PATCH'
            },
            bodyParams: [
              {
                key: 'status',
                value: 'active'
              },
            ],
            confirm: {
              title: 'Change Status',
              message: 'Are you sure you want to activate?',
              buttonText: {
                confirm: 'Confirm',
                cancel: 'Cancel'
              }
            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'inactive' }
            }
          },
          icon: { 
            type: CanIconType.Material, 
            name: 'thumb_up_al', 
            tooltip: 'change status', 
            color:  '#10d817'
          }
        },
        {
          action: {
            actionType: 'ajax',
            api: {
              apiPath: '/faqs/${id}',
              method: 'PATCH'
            },
            bodyParams: [
              {
                key: 'status',
                value: 'inactive'
              },
            ],
            confirm: {
              title: 'Change Status',
              message: 'Are you sure you want to deactivate?',
              buttonText: {
                confirm: 'Confirm',
                cancel: 'Cancel'
              }
            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'active' }
            }
          },       
          icon: { 
            type: CanIconType.Material, 
            name: 'thumb_down_al', 
            tooltip: 'change status', 
            color:  '#ee3f37'
          } 
        },
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditFaqComponent,
              inputData: [
                {
                  inputKey: 'faqId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit FAQ',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit FAQ',
          }
        },
        {
          action: {
            actionType: 'modal',
            modal: {
  
             component:  DetailFaqComponent ,
              inputData: [
                {
                  inputKey: 'faqId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'FAQ Details',
              width: 500,
            }, 
          },
          icon: {
            type: CanIconType.Material,
            name: 'visibility',
            tooltip: 'View',
            color: 'green'
          }
        },
      ],
      api: {
        apiPath: '/faqs',
        method: 'GET',
        params : new HttpParams()
        .append('productId',this.productId)
        .append('include', JSON.stringify([{  all: true  }]))
      },
      countApi:{
        apiPath:'/faqs/count',
        method:'GET'
      },
      apiDataKey: "data",
  
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Frequently Asked Questions',
    }
    
  }

}
