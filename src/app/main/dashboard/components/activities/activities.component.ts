import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { ActivityDetailsComponent } from './activity-details/activity-details.component';
import { CreateActivityComponent } from './create-activity/create-activity.component';
import { EditActivityComponent } from './edit-activity/edit-activity.component';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {

  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
 
  constructor() {
    // Fetching Path Params
   }

  ngOnInit() {
      // Table Data Init
      this.tableConfig = {
        discriminator: 'tableConfig',
        displayedColumns: [

          {
            header : 'name',
            type : 'text',
            value:'name'
          },
          {
            header: 'Icon',
            type: 'image',
            images: {
              showAll: true,
              openType: 'modal',
              imageItems: [
                {
                  type: 'api',
                  isArray: false,
                  alt: 'Preview',
                  value: 'icon'
                }
              ]
            }
          },
          {
            header: 'Status',
            type: 'enum_icon',
            value: 'status',
            enumIcons: [
              { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
              { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },
  
            ]
          },

        ],
        fieldActions: [
  
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditActivityComponent,
                inputData: [
                  {
                    inputKey: 'activityId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Edit Activity',
                width: 600
              } ,
              permission: { type: 'single', match: { key: 'UPDATE_ACTIVITIES', value: true } },
            },
            icon: {
              name: 'edit',
              tooltip: 'Edit Activity',
            }
          },
          {
            action: {
              actionType: 'ajax',
              api: {
                apiPath: '/activities/${id}',
                method: 'PATCH'
              },
              bodyParams: [
                {
                  key: 'status',
                  value: 'active'
                },
              ],
              confirm: {
                title: 'Change Status',
                message: 'Are you sure you want to active activity?',
                buttonText: {
                  confirm: 'Confirm',
                  cancel: 'Cancel'
                }
              },
              permission: { type: 'single', match: { key: 'UPDATE_ACTIVITIES', value: true } },

              displayCondition: {
                type: 'single',
                match: { operator: 'equals', key: 'status', value: 'inactive' }
              }
            },
            icon: { 
              type: CanIconType.Material, 
              name: 'thumb_up_al', 
              tooltip: 'change status', 
              color:  '#10d817'
            }
          },
          {
            action: {
              actionType: 'ajax',
              api: {
                apiPath: '/activities/${id}',
                method: 'PATCH'
              },
              bodyParams: [
                {
                  key: 'status',
                  value: 'inactive'
                },
              ],
              confirm: {
                title: 'Change Status',
                message: 'Are you sure you want to inactive activity?',
                buttonText: {
                  confirm: 'Confirm',
                  cancel: 'Cancel'
                }
              },
              permission: { type: 'single', match: { key: 'UPDATE_ACTIVITIES', value: true } },
              displayCondition: {
                type: 'single',
                match: { operator: 'equals', key: 'status', value: 'active' }
              }
            },       
            icon: { 
              type: CanIconType.Material, 
              name: 'thumb_down_al', 
              tooltip: 'change status', 
              color:  '#ee3f37'
            } 
          },
      
        ],
        filters: [
         
          {
            filtertype: "api",
            placeholder: "Status",
            type: "dropdown",
            key: "status",
            value: [
              { value: "active", viewValue: "Active" },
              { value: "inactive", viewValue: "Inactive" }
            ]
          },
       
        ],
        api: {
          apiPath: '/activities',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{ all: true }])),
        },
        apiDataKey: 'data',
        countApi: {
          apiPath: '/activities/count',
          method: 'GET',
          params : new HttpParams()
          .append('include', JSON.stringify([{  all: true  }]))
        },
        countApiDataKey: 'data.count',
        pagination: {
          pageSizeOptions: [50, 100]
        },
        header: 'Activity'
      }

      // Fab Button Config
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Create New'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateActivityComponent,
        inputData: [],
        width: 600,
        header: 'Add Activity'
      },
      permission: { type: 'single', match: { key: 'CREATE_ACTIVITIES', value: true } }
    
    }
  }

}
