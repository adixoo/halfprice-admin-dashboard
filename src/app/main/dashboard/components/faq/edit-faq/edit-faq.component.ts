import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-faq',
  templateUrl: './edit-faq.component.html',
  styleUrls: ['./edit-faq.component.scss']
})
export class EditFaqComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() faqId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: "title",
          type: "text",
          placeholder: "Title",
          value:"title",
          required: {
            value: true,
            errorMessage: "Title is required"
          },
        },
        {
          name: "type",
          type: "select",
          placeholder: "Type",
          value:'type',
          values: [
            { value: 'video', viewValue: 'Video' },
            { value: 'text', viewValue: 'Text' },
            { value: 'image', viewValue: 'Image' },
          ],
          required: {
            value: true,
            errorMessage: 'Type is required.'
          }
        },
        {
          name: "description",
          type: "textarea",
          placeholder: "Description",
          value:"description",
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'text' }
          }
          
        },
     
        {
          name: 'videos',
          type: 'document',
          value:'videos',
          placeholder: 'Upload  Video',
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST"
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'video/*',
            limit: 2,
            multipleSelect: true,
            
          },
          suffixIcon: { name: 'attach_file' },
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'video' }
          }
        },
        {
          name: 'images',
          type: 'image',
          value:'images',
          placeholder: 'Upload  Image',
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST"
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 2,
            multipleSelect: true,
            
          },
          suffixIcon: { name: 'attach_file' },
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'image' }
          }
        },
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          value:'productId',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        }, 
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        
      ],
      getApi:{
        apiPath: `/faqs/${this.faqId}`,
        method:'GET'
      },
      apiDataKey: "data",
      submitApi: {
        apiPath: `/faqs/${this.faqId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "FAQ updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
