import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateFaqComponent } from './create-faq/create-faq.component';
import { DetailFaqComponent } from './detail-faq/detail-faq.component';
import { EditFaqComponent } from './edit-faq/edit-faq.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  public tableData: CanTable;
  public fabButtonConfig: CanFabButton;
  constructor() { }

  ngOnInit() {

    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Title',
          type: 'text',
          value: 'title'
        },
        {
          header: 'Product',
          type: 'text',
          value: 'product.name'
        },
        {
          header: 'Created At',
          type: 'date',
          value: 'createdAt',
          dateDisplayType: 'dd/MM/yyyy'
        },
        {
          header:'Updated At',
          type:'date',
          value :'updatedAt',
          dateDisplayType: 'dd/MM/yyyy'
        },
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'status',
          enumIcons: [
            { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
            { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

          ]
        },
      ],
      filters: [
        {
          filtertype: 'local',
          placeholder: 'Search',
          keys: ['title', 'description']
        },
        {
          filtertype: "api",
          placeholder: "Status",
          type: "dropdown",
          key: "status",
          value: [
            { value: "active", viewValue: "Active" },
            { value: "inactive", viewValue: "Inactive" },
          ],
        }
      ],
      fieldActions: [
        {
          action: {
            actionType: 'ajax',
            api: {
              apiPath: '/faqs/${id}',
              method: 'PATCH'
            },
            bodyParams: [
              {
                key: 'status',
                value: 'active'
              },
            ],
            confirm: {
              title: 'Change Status',
              message: 'Are you sure you want to activate?',
              buttonText: {
                confirm: 'Confirm',
                cancel: 'Cancel'
              }
            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'inactive' }
            }
          },
          icon: { 
            type: CanIconType.Material, 
            name: 'thumb_up_al', 
            tooltip: 'change status', 
            color:  '#10d817'
          }
        },
        {
          action: {
            actionType: 'ajax',
            api: {
              apiPath: '/faqs/${id}',
              method: 'PATCH'
            },
            bodyParams: [
              {
                key: 'status',
                value: 'inactive'
              },
            ],
            confirm: {
              title: 'Change Status',
              message: 'Are you sure you want to deactivate?',
              buttonText: {
                confirm: 'Confirm',
                cancel: 'Cancel'
              }
            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'active' }
            }
          },       
          icon: { 
            // name: 'edit', 
            // tooltip: 'change status', 
            type: CanIconType.Material, 
            name: 'thumb_down_al', 
            tooltip: 'change status', 
            color:  '#ee3f37'
          } 
        },
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditFaqComponent,
              inputData: [
                {
                  inputKey: 'faqId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit FAQ',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit FAQ',
          }
        },
        {
          action: {
            actionType: 'modal',
            modal: {
  
             component:  DetailFaqComponent ,
              inputData: [
                {
                  inputKey: 'faqId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'FAQ Details',
              width: 500,
            }, 
          },
          icon: {
            type: CanIconType.Material,
            name: 'visibility',
            tooltip: 'View',
            color: 'green'
          }
        },
      ],
      api: {
        apiPath: '/faqs',
        // apiType: ApiType.Loopback,
        method: 'GET',
        params : new HttpParams()
        .append('include', JSON.stringify([{  all: true  }]))
      },
      countApi:{
        apiPath:'/faqs/count',
        method:'GET'
      },
      apiDataKey: "data",
  
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      header: 'Frequently Asked Questions',
    }
    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Add Form'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateFaqComponent,
        inputData: [],
        width: 600,
        header: 'Add FAQ'
      },
  
    }
  }

}
