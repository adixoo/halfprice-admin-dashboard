import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-faq',
  templateUrl: './create-faq.component.html',
  styleUrls: ['./create-faq.component.scss']
})
export class CreateFaqComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  constructor(
  ) {}

  async ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      sendAll: true,
      columnPerRow: 1,
      formFields: [
        {
          name: "title",
          type: "text",
          placeholder: "Title",
          required: {
            value: true,
            errorMessage: "Title is required"
          },
        },
        {
          name: "type",
          type: "select",
          placeholder: "Type",
          values: [
            { value: 'video', viewValue: 'Video' },
            { value: 'text', viewValue: 'Text' },
            { value: 'image', viewValue: 'Image' },
          ],
          required: {
            value: true,
            errorMessage: 'Type is required.'
          }
        },
        {
          name: "description",
          type: "textarea",
          placeholder: "Description",
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'text' }
          }
        },
       
        {
          name: 'videos',
          type: 'document',
          placeholder: 'Upload  Video',
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST"
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'video/*',
            limit: 2,
            multipleSelect: true,
            
          },
          suffixIcon: { name: 'attach_file' },
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'video' }
          }
        },
       
        {
          name: 'images',
          type: 'image',
          placeholder: 'Upload  Image',
          file: {
            api: {
              apiPath: "/files/upload",
              method: "POST"
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 2,
            multipleSelect: true,
            
          },
          suffixIcon: { name: 'attach_file' },
          displayCondition: {
            type: 'single',
            match: { operator: 'equals', key: 'type', value: 'image' }
          }
        },
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        }, 
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        
      ],
      submitApi: {
        apiPath: "/faqs",
        method: "POST",
      },

      formButton: {
        type: "raised",
        color: "primary",
        label: "Save"
      },
      submitSuccessMessage: "FAQ submited successfully!"
    };
  }
  formSubmitted(event) {
    this.outputEvent.emit(event);
  }
}
