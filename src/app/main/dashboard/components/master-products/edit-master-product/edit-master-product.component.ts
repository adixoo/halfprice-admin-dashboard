import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-master-product',
  templateUrl: './edit-master-product.component.html',
  styleUrls: ['./edit-master-product.component.scss']
})
export class EditMasterProductComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() masterProductId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'internalSku',
          type: 'text',
          placeholder: 'Internal Sku',
          value:'internalSku',
          required: {
            value: true,
            errorMessage: 'Internal Sku is required.'
          }
        },
        {
          name: 'externalSku',
          type: 'text',
          placeholder: 'External Sku',
          value:'externalSku',
          required: {
            value: true,
            errorMessage: 'External Sku is required.'
          }
        },
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'brandName',
          type: 'text',
          placeholder: 'Brand Name',
          value:'brandName',
          required: {
            value: true,
            errorMessage: 'Brand Name is required.'
          }
        },
        {
          name: "categoryId",
          type: "autocomplete",
          placeholder: "Category",
          value:'categoryId',
          required: {
            value: true,
            errorMessage: "Category is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/categories",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        {
          name: 'variantCount',
          type: 'number',
          placeholder: 'Variant Count',
          value:'variantCount',
          required: {
            value: true,
            errorMessage: 'Variant Count is required.'
          }
        },
        {
          name: 'availableStock',
          type: 'number',
          placeholder: 'Available Stock',
          value:'availableStock',
          required: {
            value: true,
            errorMessage: 'Available Stock is required.'
          }
        },
        
      ],
      getApi:{
        apiPath: `/master-products/${this.masterProductId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/master-products/${this.masterProductId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Master products updated successfully!"
    }
  }


  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
