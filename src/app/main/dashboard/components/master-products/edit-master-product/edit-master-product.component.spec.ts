import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMasterProductComponent } from './edit-master-product.component';

describe('EditMasterProductComponent', () => {
  let component: EditMasterProductComponent;
  let fixture: ComponentFixture<EditMasterProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMasterProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMasterProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
