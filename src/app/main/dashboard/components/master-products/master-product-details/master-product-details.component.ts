import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { ProductsComponent } from '../../products/products.component'
@Component({
  selector: 'app-master-product-details',
  templateUrl: './master-product-details.component.html',
  styleUrls: ['./master-product-details.component.scss']
})
export class MasterProductDetailsComponent implements OnInit {

  public detailViewData: CanDetailView;
  @Input() masterProductId: any;
  dataSource : any
  public tabViewConfig: CanTabView;
   
  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.masterProductId = parseInt(paramMap.get('id')));
  }
  ngOnInit() {
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      // dataSource : this.dataSource,
      labelPosition: 'inline',
      displayedFields: [
        {
          header: 'Internal Sku',
          type: 'text',
          value: 'internalSku',
        },
        {
          header: 'External Sku',
          type: 'text',
          value: 'externalSku',
        },
        {
          header : 'Name',
          type : 'text',
          value:'name'
        },
        // {
        //   header : 'Description',
        //   type :'text',
        //   value :'description'
        // },
        {
          header:'Status',
          type : 'text',
          value: 'status'
        },
        {
          header :'Variant Count',
          type : 'text',
          value : 'variantCount'
        },
        // {
        //   header : 'Available Stock',
        //   type : 'text',
        //   value : 'availableStock'
        // },
        {
          header : 'Created At',
          type : 'date',
          value : 'createdAt',
          dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
        },
        {
          header : 'Updated At',
          type : 'date',
          value : 'updatedAt',
          dateDisplayType : 'dd/MM/yyyy hh:mm:ss'
        }
      
      ],
      api: {
        apiPath: `/master-products/${this.masterProductId}`,
        method: 'GET'
      },
     
      // header: "Master Products Details"
    }
    
  }
  // onGetData(data) {
  //   this.tabViewConfig = {
  //     lazyLoading: true,
  //     tabs: []
  //   };
  //   this.tabViewConfig.tabs.push({
  //     component: ProductsComponent,
  //     label: 'Product',
  //     inputData: [{
  //       key: 'masterProductId',
  //       value: this.masterProductId
  //     }]
  //   })
   

  // }

}
