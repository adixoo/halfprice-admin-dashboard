import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMasterProductComponent } from './create-master-product.component';

describe('CreateMasterProductComponent', () => {
  let component: CreateMasterProductComponent;
  let fixture: ComponentFixture<CreateMasterProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMasterProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMasterProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
