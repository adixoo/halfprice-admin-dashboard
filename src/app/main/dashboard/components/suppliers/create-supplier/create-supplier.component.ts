import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanNotificationService } from 'src/@can/services/notification/notification.service';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-supplier',
  templateUrl: './create-supplier.component.html',
  styleUrls: ['./create-supplier.component.scss']
})
export class CreateSupplierComponent implements OnInit {

  formData: CanForm;
  @Input() businessId:any;
  @Output() outputEvent = new EventEmitter<boolean>(false);

  constructor(
  ) { }

  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required!'
          },
          pattern: {
            value: "^[a-zA-Z_ ]*$",
            errorMessage: 'Name must be in proper case.'
          },
  
        },

        {
          name: 'mobile',
          type: 'text',
          placeholder: 'Mobile Number',
          pattern: {
            value: '^[6-9]\\d{9}$',
            errorMessage: 'Invalid Mobile Number.'
          },
        },

        {
          name: 'email',
          type: 'text',
          placeholder: 'Email',
          pattern: {
            value: '^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
            errorMessage: 'Invalid Email.'
          }
        },

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ]
        },
        {
          name :'type',
          type:'hidden',
          placeholder:null,
          defaultValue:'supplier'
        },
        {
          name:'businessId',
          type:'hidden',
          placeholder:null,
          defaultValue:this.businessId
        }

      ],
      submitApi: {
        apiPath: `/users`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Supplier user created successfully!"
    }
    
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
