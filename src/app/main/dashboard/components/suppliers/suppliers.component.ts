import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateSupplierComponent } from './create-supplier/create-supplier.component';
import { EditSupplierComponent } from './edit-supplier/edit-supplier.component';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {

  constructor() { }
  public tableData: CanTable;
  @Input() businessId:any;
  public fabButtonConfig: CanFabButton;

  ngOnInit() {
    // Table Data Init
    this.tableData = {
      discriminator: 'tableConfig',
      displayedColumns: [
        {
          header: 'Name',
          type: 'text',
          value: 'firstName middleName lastName',
          keySeparators: [' ']
        },

        {
          header: 'Phone',
          type: 'text',
          value: 'mobile',
        },
        {
          header: 'Email',
          type: 'text',
          value: 'email',
        },
        {
          header: 'Created At',
          type: 'date',
          value: 'createdAt',
          dateDisplayType: 'dd/MM/yy'
        },
    
      ],
      fieldActions: [
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditSupplierComponent,
              inputData: [
                {
                  inputKey: 'userId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Edit Supplier User',
              width: 600
            }
          },
          icon: {
            name: 'edit',
            tooltip: 'Edit Internal User',
          }
        }
      ],
      filters: [
        {
          filtertype: 'api',
          placeholder: 'Search Name/Phone No.',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['firstName', 'lastName', 'email', 'mobile'],
            api: {
              apiPath: '/users',
              method: 'GET',
              params: new HttpParams()
              .append('businessId',this.businessId).append('type', 'supplier')
            },

          },
        },

        {
          filtertype: 'api',
          placeholder: ' Select Status',
          type: 'dropdown',
          key: 'status',
          value: [
            { value: 'active', viewValue: 'Active' },
            { value: 'inactive', viewValue: 'Inactive' },
          ],
        },

        {
          filtertype: 'api',
          placeholder: 'Creation Date',
          type: 'date',
          key: 'createdAt',
          date: {
            enabledRange: true,
            enabledTime: false,
            dateRange: {
              keys: {
                from: 'from',
                to: 'to',
              },
            },
          },
        },
      ],
      api: {
        apiPath: '/users',
        method: 'GET',
        params: new HttpParams()
        .append('businessId',this.businessId)
        .append('type', 'supplier')
      },
      countApiDataKey:'count',
      countApi: {
        apiPath: '/users/count',
        method: 'GET',
        params: new HttpParams().append('type', 'supplier')
      },
      pagination: {
        pageSizeOptions: [50, 100]
      },
      // header: 'Supplier Users'
    }
  

    this.fabButtonConfig = {
      icon: {
        name: 'add',
        tooltip: 'Add Supplier User'
      },
      type: 'modal',
      position: CanFabButtonPosition.BottomRight,
      modal: {
        component: CreateSupplierComponent,
        inputData: [{
          inputKey:'businessId',
          type:'fixed',
          value:this.businessId
        }],
        width: 600,
        header: 'Add Supplier User'
      }

    }
  }

}
