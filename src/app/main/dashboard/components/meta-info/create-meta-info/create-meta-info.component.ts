import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-meta-info',
  templateUrl: './create-meta-info.component.html',
  styleUrls: ['./create-meta-info.component.scss']
})
export class CreateMetaInfoComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;

  constructor(
  ) {}

  async ngOnInit() {
    this.formData = {
      type: "create",
      discriminator: "formConfig",
      sendAll: true,
      columnPerRow: 1,
      formFields: [
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        },
        
      {
        name: 'tnc',
        type: 'group',
        placeholder: 'Terms and Conditions',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'privacy',
        type: 'group',
        placeholder: 'Privacy',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'features',
        type: 'group',
        placeholder: 'Features',
        formFields: [         
          {
            name: "type",
            type: "select",
            placeholder: "Type",
            values: [
              { value: 'html', viewValue: 'HTML' },
              { value: 'text', viewValue: 'TEXT' },
              { value: 'url', viewValue: 'URL' },
            ]
          },
          {
            name: 'data',
            type: 'textarea',
            placeholder: 'Data'
          }
        ]
      },
      {
        name: 'default',
        type: 'toggle',
        placeholder: 'Default',
        defaultValue: false
      },
      ],
      submitApi: {
        apiPath: "/meta-infos",
        method: "POST",
      },

      formButton: {
        type: "raised",
        color: "primary",
        label: "Save"
      },
      submitSuccessMessage: "Meta information submited successfully!"
    };
  }
  formSubmitted(event) {
    this.outputEvent.emit(event);
  }

}
