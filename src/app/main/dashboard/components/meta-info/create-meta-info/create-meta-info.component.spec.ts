import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMetaInfoComponent } from './create-meta-info.component';

describe('CreateMetaInfoComponent', () => {
  let component: CreateMetaInfoComponent;
  let fixture: ComponentFixture<CreateMetaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMetaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMetaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
