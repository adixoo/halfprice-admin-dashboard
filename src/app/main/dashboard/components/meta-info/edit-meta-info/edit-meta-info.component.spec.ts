import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMetaInfoComponent } from './edit-meta-info.component';

describe('EditMetaInfoComponent', () => {
  let component: EditMetaInfoComponent;
  let fixture: ComponentFixture<EditMetaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMetaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMetaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
