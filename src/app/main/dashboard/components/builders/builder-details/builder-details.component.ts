import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { CanPermission } from 'src/@can/types/permission.type';
import { CanTabView } from 'src/@can/types/tab-view.type';
import { ProjectsComponent } from '../../projects/projects.component';

@Component({
  selector: 'app-builder-details',
  templateUrl: './builder-details.component.html',
  styleUrls: ['./builder-details.component.scss']
})
export class BuilderDetailsComponent implements OnInit {
 
  public detailViewData: CanDetailView;
  public builderId: number;
  public tabViewConfig: CanTabView;
  public projectPermission: CanPermission;

  dataSource : any

  constructor(private activedRoute: ActivatedRoute) {
    // Fetching Path Params
    this.activedRoute.paramMap.subscribe(paramMap => this.builderId = parseInt(paramMap.get('id')));
  }

  ngOnInit() {
    this.projectPermission = { type: 'single', match: { key: 'READ_PROJECTS', value: true }}
    
    
     // Detail View Config Init
     this.detailViewData = {
      discriminator: 'detailViewConfig',
      columnPerRow: 3,
      // dataSource : this.dataSource,
      labelPosition: 'inline',
      header:'Builder Details',
      displayedFields: [
        {
          header: 'Name',
          type: 'text',
          value: 'name',
        },
        {
          header: 'Status',
          type: 'text',
          value: 'status',
        },
        {
          header:'Registered Name',
          type:'text',
          value:'registeredName'
        },
        {
          header: 'Logo',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'logo'
              }
            ]
          }
        },
        {
          header: 'Banner',
          type: 'image',
          images: {
            showAll: true,
            openType: 'modal',
            imageItems: [
              {
                type: 'api',
                isArray: false,
                alt: 'Preview',
                value: 'banner'
              }
            ]
          }
        }
      ],
      api: {
        apiPath: `/builders/${this.builderId}`,
        method: 'GET'
      }
    }

  }

  onGetData(data) {
    this.tabViewConfig = {
      lazyLoading: true,
      tabs: []
    };
    this.tabViewConfig.tabs.push({
      component: ProjectsComponent,
      label: 'Project',
      inputData: [{
        key: 'builderId',
        value: this.builderId
      }]
    })
   

  }

}
