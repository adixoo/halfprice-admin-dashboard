import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBuilderComponent } from './edit-builder.component';

describe('EditBuilderComponent', () => {
  let component: EditBuilderComponent;
  let fixture: ComponentFixture<EditBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
