import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-builder',
  templateUrl: './edit-builder.component.html',
  styleUrls: ['./edit-builder.component.scss']
})
export class EditBuilderComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() builderId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [        
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'registeredName',
          type: 'text',
          placeholder: 'Registered Name',
          value:'registeredName',
          required: {
            value: true,
            errorMessage: 'Registered Name is required.'
          }
        },
        {
          name: 'logo',
          type: 'image',
          placeholder: 'Upload logo image',
          value:'logo',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'banner',
          type: 'image',
          placeholder: 'Upload Banner Image',
          value:'banner',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        

        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/builders/${this.builderId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/builders/${this.builderId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Builder updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
