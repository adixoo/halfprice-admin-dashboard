import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { ApiType, CanApi } from 'src/@can/types/api.type';
import { CanFabButton, CanFabButtonPosition } from 'src/@can/types/fab-button.type';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { CreateLeadsComponent } from './create-leads/create-leads.component';
import { EditLeadComponent } from './edit-lead/edit-lead.component';
import { UpdateLeadStatusComponent } from './update-lead-status/update-lead-status.component';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})
export class LeadsComponent implements OnInit {

 
  constructor(private apiService : CanApiService) { }
  public tableConfig: CanTable;
  public fabButtonConfig: CanFabButton;
  public statuses:any = []
  public projects:any = []


 async ngOnInit() {
    this.statuses =  await this.getLeadStatues();
    this.projects = await this.getProjects()
    // Table Data Init
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns:[
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'leadStatus',
          enumIcons: [
            { value: 'new', icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'New', color: '#5edeec'} },
            { value: 'DND',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'DND', color: '#f29506'} },
            { value: 'Not Interested',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Not Interested', color:  '#b7b2ac'} },
            { value: 'Prospect',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Prospect', color:  '#fbf513'} },
            { value: 'Success',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Success', color:  '#6bfb13'} },
            { value: 'Call Later',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Call Later', color:  '#c31313de'} },

          ]
        },
        {
          header: 'Name',
          type: 'text',
          value: 'firstName lastName',
          keySeparators:[' ']
        },
        {
          header: 'Mobile',
          type: 'text',
          value: 'mobile',
        },
        {
          header: 'Alternative Mobile',
          type: 'text',
          value: 'altMobile',
        },
        {
          header:'Configuration',
          type:'text',
          value:'configuration'
        },
        // {
        //   header:'Lead Status',
        //   type:'text',
        //   value:'leadStatus'
        // },
        {
          header: 'Follow Ups',
          type: 'date',
          value: 'nextFollowup',
        },
        {
          header: 'Email',
          type: 'text',
          value: 'email',
        },
        {
          header:'Project Name',
          type:'text',
          value:'projectName'
        },
        {
          header: 'Builder Name',
          type: 'text',
          value: 'builderName',
        },
        {
          header:'Source',
          type:'text',
          value:'type'
        },
        
        // {
        //   header: 'Unit Name',
        //   type: 'text',
        //   value: 'unitName',
        // },
        // {
        //   header: 'Package Type',
        //   type: 'text',
        //   value: 'packageType',
        // },
        // {
        //   header: 'Cart Status',
        //   type: 'text',
        //   value: 'cartStatus',
        // },
        // {
        //   header: 'totalAmount',
        //   type: 'text',
        //   value: 'totalAmount',
        // },
        // {
        //   header: 'Status',
        //   type: 'text',
        //   value: 'status',
        // },
    
    
        // {
        //   header: 'Order Amount',
        //   type: 'text',
        //   value: 'orderAmount',
        // },
        // {
        //   header: 'Order Status',
        //   type: 'text',
        //   value: 'orderStatus',
        // },
      
      ],
      // fieldActions: [

        
        
      //   {
      //     action: {
      //       actionType: 'link',
      //       link: {
      //         url: '/dashboard/internal/${id}',
      //         target: 'self',
      //         type: 'url'
      //       }
      //     },
      //     icon: {
      //       type: CanIconType.Flaticon,
      //       name: 'flaticon-eye',
      //       tooltip: 'View'
      //     }
      //   },
      // ],
      filters: [
        // {
        //   filtertype: 'local',
        //   placeholder: 'Search',
        //   keys: ['name', 'email', 'mobile']
        // },
        {
          filtertype: 'api',
          placeholder: 'Search',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'mobile-firstName lastName',
            viewValueSeparators:[' ',"-"],
            autocompleteParamKeys: ['firstName', 'email', 'mobile'],
            api: {
              apiPath: '/leads',
              method: 'GET',
            }
          },
        },
        {
          filtertype: 'api',
          placeholder: 'Alt Mobile',
          type: 'text',
          key: 'id',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'id',
            apiViewValueKey: 'altMobile-firstName',
            viewValueSeparators:['-'],
            autocompleteParamKeys: ['altMobile'],
            api: {
              apiPath: '/leads',
              method: 'GET',
            }
          },
        },
        {
          filtertype: 'api',
          placeholder: 'Configuration',
          type: 'text',
          key: 'configuration',
          searchType: 'autocomplete',
          autoComplete: {
            type: 'api',
            apiValueKey: 'name',
            apiViewValueKey: 'name',
            autocompleteParamKeys: ['name'],
            api: {
              apiPath: '/configurations',
              method: 'GET',
            }
          },
        },
        {
          filtertype: 'api',
          placeholder: 'Project Name',
          type: 'dropdown',
          key: 'projectName',
          value:this.projects,
          multipleSelect:true
          // searchType: 'autocomplete',
          // autoComplete: {
          //   type: 'api',
          //   apiValueKey: 'name',
          //   apiViewValueKey: 'name',
          //   autocompleteParamKeys: ['name'],
          //   api: {
          //     apiPath: '/projects',
          //     method: 'GET',
          //   }
          // },
        },
        {
          filtertype: 'api',
          placeholder: 'Source',
          type: 'dropdown',
          key: 'type',
          multipleSelect:true,
          value: [
            { value: 'walkIn', viewValue: 'WalkIn' },
            { value: 'website', viewValue: 'Website' },
            { value: 'callback', viewValue: 'Callback' },
            { value: 'purchase', viewValue: 'Purchase' },
            { value: 'lead', viewValue: 'Lead' },
            { value: 'support', viewValue: 'Support' }


          ]
        },
        {
          filtertype: 'api',
          placeholder: 'Lead Status',
          type: 'dropdown',
          key: 'leadStatus',
          multipleSelect:true,
          value:this.statuses
        },
        // {
        //   filtertype: 'api',
        //   placeholder: 'Lead Date',
        //   type: 'date',
        //   key: 'createdAt',
        //   date: {
        //     enabledRange: true,
        //     enabledTime: false,
        //     dateRange:{
        //       maxDate : new Date()
        //     }
        //   }
        // },
        // {
        //   filtertype: 'api',
        //   placeholder: 'Updated on',
        //   type: 'date',
        //   key: 'updatedAt',
        //   date: {
        //     enabledRange: true,
        //     enabledTime: false,
        //     dateRange:{
        //       maxDate : new Date()
        //     }
        //   }
        // },
        {
          filtertype: 'api',
          placeholder: 'Follow ups',
          type: 'date',
          key: 'nextFollowup',
          date: {
            enabledRange: true,
            enabledTime: false,        
          }
        },
        {
          filtertype: 'api',
          type: 'download',
          placeholder: 'Download',
          download: {
            downloadType: "api",
            extension: ".xlsx",
            api: {
              apiPath: '/leads',
              responseType: "blob",
              method: "GET",
              params: new HttpParams()  
                .append('order', JSON.stringify([['updatedAt', 'DESC']]))
                // .append('include',JSON.stringify({all : true}))
                .append('exportExcel', JSON.stringify({ headerDisplayType: 'propercase' ,keys :[
                  {
                    name : 'id',
                    transformedName:'Id'
                  },
                  {
                    name : 'firstName',
                    transformedName:'First Name'
                  },
                  {
                    name : 'lastName',
                    transformedName:'Last Name'
                  },
                  {
                    name:'mobile',
                    transformedName:'Mobile'
                  },
                  {
                    name:'altMobile',
                    transformedName:'Alternative Mobile'
                  },
                  {
                    name:'configuration',
                    transformedName:'Configuration'
                  },
                  {
                    name:'nextFollowup',
                    transformedName:'Follow up'
                  },
                  {
                    name: 'email',
                    transformedName: 'Email',
                  },
                  {
                    transformedName:'Project Name',
                    name:'projectName'
                  },
                  {
                    transformedName: 'Builder Name',
                    name: 'builderName',
                  },
                  {
                    transformedName:'Source',
                    name:'type'
                  },
                  {
                    transformedName:'Status',
                    name:'leadStatus'
                  },
                  {
                    name:'createdAt',
                    transformedName:'Created At'
                  },
                  {
                    name:'updatedAt',
                    transformedName:'Updated At ' 
                  }
                ] })),
            },
          }
        }
      ],
      fieldActions: [
  
        {
          action: {
            actionType: 'modal',
            modal: {
              component: EditLeadComponent,
              inputData: [
                {
                  inputKey: 'leadId',
                  type: 'key',
                  key: 'id'
                }
              ],
              header: 'Update Lead',
              width: 600
            },
            permission:{
              type:'single',
              match:{ key: 'UPDATE_LEADS', value: true }
            }

          },
          icon: {
            name: 'edit',
            tooltip: 'Update Lead',
          }
        },
        {
          action: {
            actionType: 'link',
            link: {
              url: '/dashboard/leads/${id}',
              target: 'external',
              type: 'url'
            }
          },
          icon: {
            type: CanIconType.Flaticon,
            name: 'flaticon-eye',
            tooltip: 'View'
          }
        },
     
      ],
      api: {
        apiPath: '/leads',
        method: 'GET',
        params: new HttpParams().append('order', JSON.stringify([['updatedAt', 'DESC']]))
      },
      countApi: {
        apiPath: '/leads/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 , 200]
      },
      header: 'Leads'
    }

      // Fab Button Config
      this.fabButtonConfig = {
        icon: {
          name: 'add',
          tooltip: 'Create New'
        },
        type: 'modal',
        position: CanFabButtonPosition.BottomRight,
        modal: {
          component: CreateLeadsComponent,
          inputData: [],
          width: 600,
          header: 'Create Lead'
        },
      }
  }

  getLeadStatues(){
   return new Promise((resolve, reject) =>{
      const getApi:CanApi = {
        apiPath:`/lead-statuses`,
        method:'GET'
      }
      this.apiService.request(getApi).subscribe((statusesData : any) =>{
        const statuses = []
        for (let index = 0; index < statusesData.leads.length; index++) {
          statuses.push({
            value:statusesData.leads[index]['status'],
            viewValue:statusesData.leads[index]['status'].toUpperCase()
          })     
        }
        resolve(statuses)
      }) 
  })
}

getProjects(){
  return new Promise((resolve, reject) =>{
     const getApi:CanApi = {
       apiPath:`/projects`,
       method:'GET'
     }
     this.apiService.request(getApi).subscribe((res : any) =>{
       const projects = []
       for (let index = 0; index < res.length; index++) {
        projects.push({
           value:res[index]['name'],
           viewValue:res[index]['name'].toUpperCase()
         })     
       }
       resolve(projects)
     }) 
 })
}
}
