import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanForm } from 'src/@can/types/form.type';
import { UpdateService } from '../../../orders/updates/update.service';

@Component({
  selector: 'app-update-follow-ups',
  templateUrl: './update-follow-ups.component.html',
  styleUrls: ['./update-follow-ups.component.scss']
})
export class UpdateFollowUpsComponent implements OnInit {

  @Input() followupId:number;
  @Input() leadId:number;
  @Input() status:string;
 @Output() outputEvent = new EventEmitter<boolean>(false);


  constructor( 
    private apiService: CanApiService,
    private updateService : UpdateService,
    ){

  }
  // constructor(
  //   public dialogRef: MatDialogRef<UpdateFollowUpsComponent>,
  //   @Inject(MAT_DIALOG_DATA) public data: any) {
  //     // this.leadId = this.data.leadId
  //   }
  formData: CanForm;

  ngOnInit() {
    // this.closeModal();
   
  }
  // closeModal(){
  //   this.dialogRef.close();
  // }

  onConfirm(){
    const submitApi:CanApi = {
      apiPath:`/follow-ups/${this.followupId}`,
      method:'PATCH'
    }
    const bodyData ={
      'status' : this.status
    }
    this.apiService.request(submitApi,bodyData).subscribe(res =>{
      if(res){
      this.updateLeadFollowup();
      }
    })
  }
  updateLeadFollowup(){
    const submitApi:CanApi = {
      apiPath:`/leads/${this.leadId}`,
      method:'PATCH'
    }
    const bodyData ={
      'nextFollowup' : null
    }
    this.apiService.request(submitApi,bodyData).subscribe(res =>{
      if(res){
        this.outputEvent.emit(true);
      this.updateService.isFollowUpsAdded = true;

      }
    })
  }

  onDismiss(){
    this.outputEvent.emit(false);

  }
}
