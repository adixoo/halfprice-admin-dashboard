import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CanButton, CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';
import { UpdateService } from '../../orders/updates/update.service';
import { CreateFollowUpsComponent } from './create-follow-ups/create-follow-ups.component';
import { UpdateFollowUpsComponent } from './update-follow-ups/update-follow-ups.component';

@Component({
  selector: 'app-follow-ups',
  templateUrl: './follow-ups.component.html',
  styleUrls: ['./follow-ups.component.scss']
})
export class FollowUpsComponent implements OnInit {

  constructor(
    private updateService : UpdateService,
    public dialog: MatDialog) { }

  @Input() leadId:number;
  public tableConfig: CanTable;
  public postUpdatesButtonConfig : CanButton;

  refreshEvent = new EventEmitter<boolean>(true);
  ngOnInit() {

    this.postUpdatesButtonConfig = {
      label : 'Create FollowUp',
      type : 'raised',
      color : 'primary',
      float:'left'
    } 
    this.updateService.isFollowUpsAdded$.subscribe(value => this.refreshEvent.next(value))
   
    // Table Data Init

    this.tableConfig = {
      discriminator: 'tableConfig',
      refreshEvent : this.refreshEvent,

      displayedColumns: [
        
        {
          header: 'Schedule',
          type: 'date',
          value: 'schedule',
        },
        {
          header: 'Created by ',
          type: 'text',
          value: 'createdBy.name',
        },
        {
          header: 'Updated At ',
          type: 'date',
          value: 'updatedAt',
        },
        {
          header: 'Status',
          type: 'enum_icon',
          value: 'status',
          enumIcons: [
            { value: 'pending', icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Pending', color: '#e60000'} },
            { value: 'done',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Done', color:  '#37e600'} },
            { value: 'reject',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Reject', color:  '#ece605'} },

          ]
        },
      ],
      fieldActions: [
        {
          action: {

            actionType: 'modal',
            modal: {
              component: UpdateFollowUpsComponent,
              inputData: [
                {
                  inputKey: 'followupId',
                  type: 'key',
                  key: 'id'
                },
                {
                  inputKey: 'leadId',
                  type: 'key',
                  key: 'leadId'
                },
                {
                  inputKey: 'status',
                  type: 'fixed',
                  // key: 'leadId'
                  value:'done'
                }
              ],
              header: 'Done Follow ups',
              width: 250,

            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'pending' }
            }
          },

          icon: {
            type: CanIconType.Material,
            name: ' check_box',
            tooltip: 'Done',
            color: '#101010'
          },
        },
        {
          action: {

            actionType: 'modal',
            modal: {
              component: UpdateFollowUpsComponent,
              inputData: [
                {
                  inputKey: 'followupId',
                  type: 'key',
                  key: 'id'
                },
                {
                  inputKey: 'leadId',
                  type: 'key',
                  key: 'leadId'
                },
                {
                  inputKey: 'status',
                  type: 'fixed',
                  // key: 'leadId'
                  value:'reject'
                }
              ],
              header: 'Reject Follow Ups',
              width: 250,

            },
            displayCondition: {
              type: 'single',
              match: { operator: 'equals', key: 'status', value: 'pending' }
            }
          },

          icon: {
            type: CanIconType.Material,
            name: ' thumb_down_al',
            tooltip: 'Reject',
            color: '#101010'
          },
        },
        // {
        //   action: {
        //     actionType: 'ajax',
        //     api: {
        //       apiPath: '/follow-ups/${id}' ,
        //       method: 'PATCH',
        //     },
        //     bodyParams: [
        //       {
        //         key: 'status',
        //         value: 'done'
        //       },

        //     ],
        //     confirm: {
        //       title: 'Change Status',
        //       message: 'Are you sure you want to change Status',
        //       buttonText: {
        //         confirm: 'Confirm',
        //         cancel: 'Cancel'
        //       }
        //     },
        //     displayCondition: {
        //       type: 'single',
        //       match: { operator: 'equals', key: 'status', value: 'pending' }
        //     }
        //   },
        //   icon: {
        //     type: CanIconType.Material,
        //     name: ' check_box',
        //     tooltip: 'Done',
        //     color: '#101010'

        //   },
        // },
        
      ],
      
      api: {
        apiPath: '/follow-ups',
        method: 'GET',
        params: new HttpParams()
        .append('include',JSON.stringify([{all:true}]))
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
        .append('leadId',this.leadId.toString())

      },
      countApi: {
        apiPath: '/follow-ups/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [100 , 200 ]
      },
      header: 'Follow ups'
    }
  }
  openPostModal(){
    // this.notificationService.showSuccess();
    const dialogRef = this.dialog.open(CreateFollowUpsComponent,
      {
        width: '700px',
        data: { leadId: this.leadId }
      }
    );
    dialogRef.afterClosed().subscribe(submitResponse => {
    });

  }
}
