import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanApiService } from 'src/@can/services/api/api.service';
import { CanAuthService } from 'src/@can/services/auth/auth.service';
import { CanNotificationService } from 'src/@can/services/notification/notification.service';
import { CanApi } from 'src/@can/types/api.type';
import { CanButton } from 'src/@can/types/shared.type';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  constructor(private apiService: CanApiService,  private authService: CanAuthService , private notificationService: CanNotificationService) { }
  public saveButtonConfig : CanButton;
  public comment:any
  @Input() leadId:any;
  public previousComments : any[] = [];

  ngOnInit() {
    this.getComments()
    this.saveButtonConfig = {
      label : 'Save',
      type : 'raised',
      color : 'primary'
    }
  }

  getComments(){
    return new Promise((resolve) => {
      const commentApi: CanApi = {
        apiPath: `/comments`,
        method: 'GET',
        params: new HttpParams()
        .append('include', JSON.stringify([{ all: true }]))
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
        .append('leadId',this.leadId)
      }
      this.apiService.request(commentApi)
        .subscribe((comments:any[]) => {
          this.previousComments = comments
          })
    })
  }

  saveComment(){
  
      const bodyData = {
        userId :  parseInt(this.authService.currentUser()['id']),
        leadId : this.leadId,
        comment :  this.comment
      }
      const submitApi:CanApi = {
        apiPath:`/comments`,
        method:'POST'
      }
  
      this.apiService.request(submitApi,bodyData).subscribe(res =>{
        if(res){
          this.notificationService.showError("Successfully submitted")
           this.getComments();
        }
      })
    
  }

}
