import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-update-lead-status',
  templateUrl: './update-lead-status.component.html',
  styleUrls: ['./update-lead-status.component.scss']
})
export class UpdateLeadStatusComponent implements OnInit {


  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() leadId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
      
        {
          name: 'leadStatus',
          type: 'select',
          placeholder: 'Lead status',
          value:'leadStatus',
          values: [
            { value: 'new', viewValue: 'New' },
            { value: 'onHold', viewValue: 'On Hold' },
            { value: 'cold', viewValue: 'Cold' },
            { value: 'closed', viewValue: 'Closed' }
          ],
          required: {
            value: true,
            errorMessage: 'Lead status is required.'
          }
        }
      ],
      getApi:{
        apiPath: `/leads/${this.leadId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/leads/${this.leadId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Lead status successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
