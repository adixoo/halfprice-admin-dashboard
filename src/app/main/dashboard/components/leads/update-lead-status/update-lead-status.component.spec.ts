import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLeadStatusComponent } from './update-lead-status.component';

describe('UpdateLeadStatusComponent', () => {
  let component: UpdateLeadStatusComponent;
  let fixture: ComponentFixture<UpdateLeadStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLeadStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLeadStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
