import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateConversationsComponent } from './create-conversations.component';

describe('CreateConversationsComponent', () => {
  let component: CreateConversationsComponent;
  let fixture: ComponentFixture<CreateConversationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateConversationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateConversationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
