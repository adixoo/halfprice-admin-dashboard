import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadsDetailsViewComponent } from './leads-details-view.component';

describe('LeadsDetailsViewComponent', () => {
  let component: LeadsDetailsViewComponent;
  let fixture: ComponentFixture<LeadsDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadsDetailsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadsDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
