import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CanDetailView } from 'src/@can/types/detail-view.type';
import { UpdateService } from '../../../orders/updates/update.service';
import { EditLeadComponent } from '../../edit-lead/edit-lead.component';

@Component({
  selector: 'app-leads-details-view',
  templateUrl: './leads-details-view.component.html',
  styleUrls: ['./leads-details-view.component.scss']
})
export class LeadsDetailsViewComponent implements OnInit {

  @Input() leadId:number;  
  public detailViewData: CanDetailView;
  private subscription:Subscription;
  private user:any;
  
  refreshEvent = new EventEmitter<boolean>(true);
  constructor(
    private updateService : UpdateService,
  ) { }

  ngOnInit() {

    this.subscription= this.updateService.isFollowUpsAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })

    this.updateService.isConversationAdded$.subscribe(value => {
      this.refreshEvent.next(value)
    })

      // Detail View Config Init
      this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 2,
        // dataSource: this.dataSource,
        refreshEvent : this.refreshEvent,
  
        labelPosition: 'inline',
        action:[
          {
            action: {
              actionType: 'modal',
              modal: {
                component: EditLeadComponent,
                inputData: [
                  {
                    inputKey: 'leadId',
                    type: 'key',
                    key: 'id'
                  }
                ],
                header: 'Update Lead',
                width: 600
              },
              permission:{
                type:'single',
                match:{ key: 'UPDATE_LEADS', value: true }
              }
  
            },
            icon: {
              name: 'edit',
              tooltip: 'Update Lead',
            }
          },
      ],
        displayedFields: [
          {
            header:'Lead Status',
            type:'text',
            value:'leadStatus'
          } ,
          {
            header: 'Name',
            type: 'text',
            value: 'firstName lastName',
            keySeparators:[' ']
          },
          //  {
          //   header: 'Last Name',
          //   type: 'text',
          //   value: 'lastName'
          // },
          {
            header: 'Mobile',
            type: 'text',
            value: 'mobile',
          },
          {
            header: 'Alternate Mobile',
            type: 'text',
            value: 'altMobile',
          },
          {
            header: 'Email',
            type: 'text',
            value: 'email',
          },
          {
            header:'Project Name',
            type:'text',
            value:'projectName'
          },
          {
            header: 'Builder',
            type: 'text',
            value: 'builderName',
          },
         
  
          // {
          //   header: 'Payment Status',
          //   type: 'text',
          //   value: 'paymentStatus',
          // },
  
          {
            header: 'Configuration',
            type: 'text',
            value: 'configuration',
          },
          {
            header: 'Follow Ups',
            type: 'date',
            value: 'nextFollowup',
          },
          {
            header:'Source',
            type:'text',
            value:'type'
          },
          {
            header:'Note',
            type:'text',
            value:'notes'
          }
          // {
          //   header: 'Order Amount',
          //   type: 'text',
          //   value: 'orderAmount',
          // },
          // {
          //   header: 'Order Status',
          //   type: 'text',
          //   value: 'orderStatus',
          // },
         
        ],
        api: {
          apiPath: `/leads/${this.leadId}`,
          method: 'GET',
          params : new HttpParams().append('include', JSON.stringify([{ all: true }]))
        },
        header: "Leads Details"
      }
  
  }

}
