import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-lead',
  templateUrl: './edit-lead.component.html',
  styleUrls: ['./edit-lead.component.scss']
})
export class EditLeadComponent implements OnInit {

  constructor() { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() leadId: string;
  formData: CanForm;
 

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      // sendAll:true,
      formFields: [
      
        {
          name: 'firstName',
          type: 'text',
          placeholder: 'First Name',
          value:'firstName',
          required: {
            value: true,
            errorMessage: 'First name is required.'
          }
        },
        {
          name: 'lastName',
          type: 'text',
          placeholder: 'Last Name',
          value:'lastName'
        },
        {
          name: 'altMobile',
          type: 'text',
          placeholder: 'Alternative Mobile',
          value:'altMobile',
          // pattern:{
          //   value:'^[6-9]\\d{9}$',
          //   errorMessage:'Please enter valid mobile number'
          // }
        },
        {
          name: 'email',
          type: 'text',
          placeholder: 'Email',
          value:'email',
          pattern:{
            value:'^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$',
            errorMessage:'Please enter valid email'
          }
        },
        {
                  name: 'projectName',
                  type: 'select',
                  placeholder: 'Project',
                  value:'projectName',
                  select: {
                    type: 'api',
                    api: {
                      apiPath: '/projects',
                      method: 'GET',
                      params: new HttpParams()
                      .append('include', JSON.stringify([{ all: true }]))
                    },
                    apiValueKey: "name",
                    apiViewValueKey: "name"
                  },
                  relativeSetFields:[{
                    key:'builderName',
                    type:'different',
                    value :'builder.name',
                    differentType:'dataSource'
                  }]
              
                }, 
                {
                  name:'builderName',
                  type:'hidden',
                  value:'builder',
                  placeholder:null
                },
        // {
        //   name: 'leadStatus',
        //   type: 'select',
        //   placeholder: 'Lead status',
        //   value:'leadStatus',
        //   values: [
        //     { value: 'new', viewValue: 'New' },
        //     { value: 'onHold', viewValue: 'On Hold' },
        //     { value: 'cold', viewValue: 'Cold' },
        //     { value: 'closed', viewValue: 'Closed' }
        //   ],
        //   required: {
        //     value: true,
        //     errorMessage: 'Lead status is required.'
        //   }
        // }
      ],
      getApi:{
        apiPath: `/leads/${this.leadId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/leads/${this.leadId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Lead status successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
