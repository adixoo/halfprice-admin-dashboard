import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {

  @Input() userId : any;
  public detailViewData: CanDetailView;
  constructor() { }

  ngOnInit() {
       // Detail View Config Init
       this.detailViewData = {
        discriminator: 'detailViewConfig',
        columnPerRow: 1,
        labelPosition: 'inline',
        // dataSource:this.user,
        displayedFields: [
          {
            header: 'Steps',
            type: 'text',
            value: 'steps'
          },
          {
            header: 'Sleep',
            type: 'text',
            value: 'sleep',
          },
          {
            header: 'Calories',
            type: 'text',
            value: 'calories',
          }
        ],
      api:{
        apiPath: `/user-goals`,
        method:'GET',
        params: new HttpParams()
        .append('userId',this.userId)
        .append('order', JSON.stringify([['createdAt', 'DESC']]))
        .append('include', JSON.stringify([{ all: true }]))
        
      },
      apiDataKey: 'data[0]'

      }
  }

}
