import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-wearable',
  templateUrl: './wearable.component.html',
  styleUrls: ['./wearable.component.scss']
})
export class WearableComponent implements OnInit {

  @Input() wearables : any;
  public tableConfig: CanTable;
  constructor() { }

  ngOnInit() {
    this.tableConfig = {
      discriminator: 'tableConfig',
      dataSource:this.wearables,
      displayedColumns: [
   {
     type:"text",
     header:"name",
     value:'name'
   },
   {
     header:"Model No",
     type:'text',
     value:'modelNo'
   },
   {
    header:"Mac Address",
    type:'text',
    value:'macAddr'
  },
  {
    header:"Firmware",
    type:'text',
    value:'firmware'
  }
      ],
      
      api: {
        apiPath: '/user-products',
        method: 'GET',
        params : new HttpParams()
        // .append('userId', this.userId.toString())
      },
      apiDataKey: 'data',
      countApi: {
        apiPath: '/user-products/count',
        method: 'GET',
      },
      countApiDataKey: 'count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      // header: 'Wearables'
    }
  }

}
