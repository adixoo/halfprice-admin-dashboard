import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanDetailView } from 'src/@can/types/detail-view.type';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  @Input() userId : any;
  public detailViewData: CanDetailView;

  constructor() { }

  ngOnInit() {
        // Detail View Config Init
        this.detailViewData = {
          discriminator: 'detailViewConfig',
          columnPerRow: 1,
          labelPosition: 'inline',
          // dataSource:this.user,
          displayedFields: [
            {
              header: 'Gender',
              type: 'text',
              value: 'gender'
            },
            {
              header: 'Height',
              type: 'text',
              value: 'height',
            },
            {
              header: 'Weight',
              type: 'text',
              value: 'weight',
            },
            {
              header: 'DOB',
              type: 'date',
              value: 'dob',
              dateDisplayType:'dd-MM-yyyy'
            }
          ],
        api:{
          apiPath: `/user-stats`,
          method:'GET',
          params: new HttpParams()
          .append('userId',this.userId)
          .append('order', JSON.stringify([['createdAt', 'DESC']]))
          .append('include', JSON.stringify([{ all: true }]))
          
        },
        apiDataKey: 'data[0]'
  
        }
  }

}
