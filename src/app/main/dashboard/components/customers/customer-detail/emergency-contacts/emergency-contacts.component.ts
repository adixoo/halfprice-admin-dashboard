import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CanIconType } from 'src/@can/types/shared.type';
import { CanTable } from 'src/@can/types/table.type';

@Component({
  selector: 'app-emergency-contacts',
  templateUrl: './emergency-contacts.component.html',
  styleUrls: ['./emergency-contacts.component.scss']
})
export class EmergencyContactsComponent implements OnInit {

  
  @Input() userId : any;
  public tableConfig: CanTable;
  constructor() { }

  ngOnInit() {
    this.tableConfig = {
      discriminator: 'tableConfig',
      displayedColumns: [
   {
     type:"text",
     header:"Name",
     value:'name'
   },
   {
     header:"Email",
     type:'text',
     value:'email'
   },
   {
    header:"Phone",
    type:'text',
    value:'phone'
  },
  {
    header: 'Status',
    type: 'enum_icon',
    value: 'status',
    enumIcons: [
      { value: 'active',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Active', color:  '#10d817'} },
      { value: 'inactive',icon: { type: CanIconType.Material, name: 'fiber_manual_record', tooltip: 'Inactive', color:  '#ee3f37'} },

    ]
  }
      ],
      
      api: {
        apiPath: '/emergency-contacts',
        method: 'GET',
        params : new HttpParams()
        .append('userId', this.userId.toString())
      },
      apiDataKey: 'data',
      countApi: {
        apiPath: '/emergency-contacts/count',
        method: 'GET',
      },
      countApiDataKey: 'data.count',
      pagination: {
        pageSizeOptions: [50, 100]
      },
      // header: 'devices'
    }
  }

}
