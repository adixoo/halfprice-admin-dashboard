import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-configuration',
  templateUrl: './create-configuration.component.html',
  styleUrls: ['./create-configuration.component.scss']
})
export class CreateConfigurationComponent implements OnInit {

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          required: {
            value: true,
            errorMessage: 'Description required.'
          }
        },
        {
          name: 'image',
          type: 'image',
          placeholder: 'Upload Image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        {
          name: "projectId",
          type: "autocomplete",
          placeholder: "Project",
          required: {
            value: true,
            errorMessage: "Project is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/projects",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        // {
        //   name: 'config',
        //   type: 'array',
        //   placeholder: 'Config',
        //   formArray: {
        //     addButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Add Config'
        //     },
        //     deleteButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Delete Config'
        //     }
        //   },
        //   formFields: [    
          
        //     {
        //       name: 'type',
        //       type: 'select',
        //       placeholder: 'Type',
        //       select: {
        //         type: 'api',
        //         api: {
        //           apiPath: '/room-types',
        //           method: 'GET'
        //         },
        //         apiValueKey: "name",
        //         apiViewValueKey: "name"
        //       },
        //       required: {
        //         value: true,
        //         errorMessage: 'Type is required!'
        //       }
        //     },
        //     {
        //       name: 'categories',
        //       type: 'array',
        //       placeholder: 'Categories',
        //       formArray: {
        //         addButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Add Category'
        //         },
        //         deleteButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Delete Category'
        //         }
        //       },
        //       formFields: [   
        //         {
        //           name: 'name',
        //           type: 'select',
        //           placeholder: 'Names',
        //           select: {
        //             type: 'api',
        //             api: {
        //               apiPath: '/categories',
        //               method: 'GET'
        //             },
        //             apiValueKey: "name",
        //             apiViewValueKey: "name"
        //           },
        //           required: {
        //             value: true,
        //             errorMessage: 'Name is required!'
        //           },
        //           relativeSetFields:[{
        //             key:'id',
        //             type:'different',
        //             value :'id',
        //             differentType:'dataSource'
        //           }]
              
        //         }, 
        //         {
        //           name:'id',
        //           type:'hidden',
        //           value:'id',
        //           placeholder:null
        //         },
        //         {
        //           name:'quantity',
        //           type:'number',
        //           placeholder:'Quantity',
        //           required: {
        //             value: true,
        //             errorMessage: "Quantity is required."
        //           },
        //         }
        //       ]
        //     }
        //   ]
        // },
      ],
      submitApi: {
        apiPath: `/configurations`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Configurations created successfully!"
    }

  }


  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
