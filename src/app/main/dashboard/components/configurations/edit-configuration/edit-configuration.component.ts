import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-configuration',
  templateUrl: './edit-configuration.component.html',
  styleUrls: ['./edit-configuration.component.scss']
})
export class EditConfigurationComponent implements OnInit {

  
  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() configurationId: string;
  formData: CanForm;
  constructor() { }

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Name',
          value:'name',
          required: {
            value: true,
            errorMessage: 'Name is required.'
          }
        },
        {
          name: 'description',
          type: 'text',
          placeholder: 'Description',
          value:'description',
          required: {
            value: true,
            errorMessage: 'Description required.'
          }
        },
        {
          name: 'image',
          type: 'image',
          placeholder: 'Upload Image',
          value:'image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        {
          name: "projectId",
          type: "autocomplete",
          placeholder: "Project",
          value:'projectId',
          required: {
            value: true,
            errorMessage: "Project is required."
          },
          autoComplete: {
            type: "api",
            api: {
              apiPath: "/projects",
              method: "GET",
              // params: new HttpParams().append("isInternal", "true")
            },
            autocompleteParamKeys: ["name"],
            apiValueKey: "id",
            apiViewValueKey: "name"
          }
        },
        // {
        //   name: 'config',
        //   type: 'array',
        //   placeholder: 'Config',
        //   value:'config',
        //   formArray: {
        //     addButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Add Config'
        //     },
        //     deleteButton: {
        //       type: 'raised',
        //       color: 'primary',
        //       label: 'Delete Config'
        //     }
        //   },
        //   formFields: [    
          
        //     {
        //       name: 'type',
        //       type: 'select',
        //       placeholder: 'Type',
        //       value:'type',
        //       select: {
        //         type: 'api',
        //         api: {
        //           apiPath: '/room-types',
        //           method: 'GET'
        //         },
        //         apiValueKey: "name",
        //         apiViewValueKey: "name"
        //       },
        //       required: {
        //         value: true,
        //         errorMessage: 'Type is required!'
        //       }
        //     },
        //     {
        //       name: 'categories',
        //       type: 'array',
        //       placeholder: 'Categories',
        //       value:'categories',
        //       formArray: {
        //         addButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Add Category'
        //         },
        //         deleteButton: {
        //           type: 'raised',
        //           color: 'primary',
        //           label: 'Delete Category'
        //         }
        //       },
        //       formFields: [   
        //         {
        //           name: 'name',
        //           type: 'select',
        //           placeholder: 'Names',
        //           value:'name',
        //           select: {
        //             type: 'api',
        //             api: {
        //               apiPath: '/categories',
        //               method: 'GET'
        //             },
        //             apiValueKey: "name",
        //             apiViewValueKey: "name"
        //           },
        //           required: {
        //             value: true,
        //             errorMessage: 'Name is required!'
        //           },
        //           // relatedTo:['id'],
        //           relativeSetFields:[{
        //             key:'id',
        //             type:'different',
        //             value :'id',
        //             differentType:'dataSource'
        //           }]
              
        //         }, 
        //         {
        //           name:'id',
        //           type:'hidden',
        //           placeholder:null
        //         },
        //         // {
        //         //   name: "name",
        //         //   type: "autocomplete",
        //         //   placeholder: "Name",
        //         //   required: {
        //         //     value: true,
        //         //     errorMessage: "Name is required."
        //         //   },
        //         //   autoComplete: {
        //         //     type: "api",
        //         //     api: {
        //         //       apiPath: "/categories",
        //         //       method: "GET",
        //         //       // params: new HttpParams().append("isInternal", "true")
        //         //     },
        //         //     autocompleteParamKeys: ["name"],
        //         //     apiValueKey: "name",
        //         //     apiViewValueKey: "name"
        //         //   }
        //         // },
        //         {
        //           name:'quantity',
        //           type:'number',
        //           placeholder:'Quantity',
        //           value:'quantity',
        //           required: {
        //             value: true,
        //             errorMessage: "Type is required."
        //           },
        //         }
        //       ]
        //     }
        //   ]
        // },
      ],
      getApi:{
        apiPath: `/configurations/${this.configurationId}`,
        method:'GET'
      },
      submitApi: {
        apiPath: `/configurations/${this.configurationId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Configurations updated successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
