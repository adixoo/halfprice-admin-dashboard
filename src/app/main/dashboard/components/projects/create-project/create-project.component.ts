import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanFormStepper, CanFormStepperLayout } from 'src/@can/types/form-stepper.type';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {

  formStepperConfig: CanFormStepper;
  @Input() builderId:any;
  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;
  constructor() {
   
  }

  ngOnInit() {   
    
    // this.formStepperConfig = {
    //   discriminator: 'formStepperConfig',
    //   type: 'create',
    //   layout: CanFormStepperLayout.Vertical,
    //   submitOnEachStep: true,
    //   apiIdKey: 'id',
    //   backButton: {
    //     type: 'basic',
    //     color: 'primary',
    //     label: 'Back'
    //   },
    //   submitApi: {
    //     apiPath: '/addresses',
    //     method: 'POST'
    //   },
    //   firstTimeSubmitApi: {
    //     apiPath: '/projects',
    //     method: 'POST'
    //   },
    //   steps: [
       
    //     {
    //       label: 'Project Info',
    //       form: {
    //         type: 'create',
    //         discriminator: 'formConfig',
    //         columnPerRow: 1,
    //         sendAll:true,
    //         formFields: [
    //           {
    //             name: 'name',
    //             type: 'text',
    //             placeholder: 'Name',
    //             required: {
    //               value: true,
    //               errorMessage: 'Name is required.'
    //             }
    //           },
    //           {
    //             name: 'builderId',
    //             type: 'hidden',
    //             placeholder: '',
    //             defaultValue:this.builderId
    //           },
    //          {
    //             name: 'status',
    //             type: 'select',
    //             placeholder: 'Status',
    //             values: [
    //               { value: 'active', viewValue: 'active' },
    //               { value: 'inactive', viewValue: 'inactive' },
    //             ],
    //             required: {
    //               value: true,
    //               errorMessage: 'Status is required.'
    //             }
    //         }
    //         ],
    //         formButton:
    //         {
    //           type: 'raised',
    //           color: 'primary',
    //           label: 'Next'
    //         }
    //       }
    //     },
    //     {
    //       label: 'Adderess Info',
    //       form: {
    //         type: 'create',
    //         discriminator: 'formConfig',
    //         columnPerRow: 1,
    //         formFields: [
    //         {
    //             name: 'state',
    //             type: 'autocomplete',
    //             placeholder: 'State',
    //             autoComplete: {
    //               type: 'api',
    //               api: {
    //                 apiPath: '/states',
    //                 method: 'GET'
    //               },
    //               autocompleteParamKeys: ['name'],
    //               apiValueKey: 'name',
    //               apiViewValueKey: 'name',
    //             },
    //                required: {
    //               value: true,
    //               errorMessage: 'State is required.'
    //             }
    //           },
    //           {
    //             name: 'city',
    //             type: 'autocomplete',
    //             placeholder: 'City',
    //             autoComplete: {
    //               type: 'api',
    //               api: {
    //                 apiPath: '/cities',
    //                 method: 'GET'
    //               },
    //               autocompleteParamKeys: ['name'],
    //               apiValueKey: 'name',
    //               apiViewValueKey: 'name',
    //             },
    //                required: {
    //               value: true,
    //               errorMessage: 'City is required.'
    //             }
    //           },
    //         ],
    //         formButton:
    //         {
    //           type: 'raised',
    //           color: 'primary',
    //           label: 'Next'
    //         }
    //       }
    //     },
        
    //   ]
    // }

    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      sendAll:true,
      formFields: [
        
          {
            name: 'name',
            type: 'text',
            placeholder: 'Name',
            required: {
              value: true,
              errorMessage: 'Name is required.'
            }
          },
          {
            name: 'builderId',
            type: 'hidden',
            placeholder: '',
            defaultValue:this.builderId
          },
          {
            name: 'status',
            type: 'select',
            placeholder: 'Status',
            values: [
              { value: 'active', viewValue: 'active' },
              { value: 'inactive', viewValue: 'inactive' },
            ],
            required: {
              value: true,
              errorMessage: 'Status is required.'
            }
        },
        {
          name: 'image',
          type: 'image',
          placeholder: 'Upload Image',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },

      ],
      submitApi: {
        apiPath: `/projects`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Project created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }
}
