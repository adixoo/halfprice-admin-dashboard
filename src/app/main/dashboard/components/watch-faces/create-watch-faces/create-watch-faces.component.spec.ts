import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWatchFacesComponent } from './create-watch-faces.component';

describe('CreateWatchFacesComponent', () => {
  let component: CreateWatchFacesComponent;
  let fixture: ComponentFixture<CreateWatchFacesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWatchFacesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWatchFacesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
