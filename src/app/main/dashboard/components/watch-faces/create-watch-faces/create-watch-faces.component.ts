import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-create-watch-faces',
  templateUrl: './create-watch-faces.component.html',
  styleUrls: ['./create-watch-faces.component.scss']
})
export class CreateWatchFacesComponent implements OnInit {

  constructor() { }

  @Output() outputEvent = new EventEmitter<boolean>(false);
  formData: CanForm;


  ngOnInit() {
    this.formData = {
      type: 'create',
      discriminator: 'formConfig',
      columnPerRow: 1,
      formFields: [
        {
          name: "name",
          type: "text",
          placeholder: "Name"
        },
        {
          name: 'imageUrl',
          type: 'image',
          placeholder: 'Upload Mockup',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'fileUrl',
          type: 'document',
          placeholder: 'Upload Binary',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'csv/*, pdf/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: "dialNumber",
          type: "text",
          placeholder: "Dial Number"
        },
     
        {
          name: 'type',
          type: 'select',
          placeholder: 'Type',
          values: [
            { value: 'static', viewValue: 'Static' },
            { value: 'cloud', viewValue: 'Cloud' },
            { value: 'custom', viewValue: 'Custom' },

          ],
          required: {
            value: true,
            errorMessage: 'Type is required.'
          },
        },
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        }, 
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ]
        },
        {
          name: "isEditable",
          type: "checkbox",
          placeholder: "Is Editable"
        }
      ],
      submitApi: {
        apiPath: `/watch-faces`,
        method: 'POST'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Watch Face created successfully!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }

}
