import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchFacesDatailViewComponent } from './watch-faces-datail-view.component';

describe('WatchFacesDatailViewComponent', () => {
  let component: WatchFacesDatailViewComponent;
  let fixture: ComponentFixture<WatchFacesDatailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchFacesDatailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchFacesDatailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
