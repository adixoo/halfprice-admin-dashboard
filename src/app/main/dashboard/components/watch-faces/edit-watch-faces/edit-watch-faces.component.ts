import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CanForm } from 'src/@can/types/form.type';

@Component({
  selector: 'app-edit-watch-faces',
  templateUrl: './edit-watch-faces.component.html',
  styleUrls: ['./edit-watch-faces.component.scss']
})
export class EditWatchFacesComponent implements OnInit {

  constructor() { }

 
  @Output() outputEvent = new EventEmitter<boolean>(false);
  @Input() watchFaceId: string;
  formData: CanForm;
 

  ngOnInit() {
    this.formData = {
      type: 'edit',
      discriminator: 'formConfig',
      columnPerRow: 1,
      // sendAll:true,
      formFields: [
        {
          name: "name",
          type: "text",
          placeholder: "Name",
          value:'name'
        },
        {
          name: 'imageUrl',
          type: 'image',
          placeholder: 'Upload Mockup',
          value:'imageUrl',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'image/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: 'fileUrl',
          type: 'document',
          placeholder: 'Upload Binary',
          value:'fileUrl',
          file: {
            api: {
              apiPath: '/files/upload',
              method: 'POST'
            },
            apiKey: 'data.files[0].path',
            payloadName: 'files',
            acceptType: 'csv/*, pdf/*',
            limit: 1,
            multipleSelect: false,
          },
          suffixIcon: { name: 'attach_file' },
        },
        {
          name: "dialNumber",
          type: "text",
          placeholder: "Dial Number",
          value:'dialNumber'
        },
     
        {
          name: 'type',
          type: 'select',
          placeholder: 'Type',
          value:'type',
          values: [
            { value: 'static', viewValue: 'Static' },
            { value: 'cloud', viewValue: 'Cloud' },
            { value: 'custom', viewValue: 'Custom' },

          ],
          required: {
            value: true,
            errorMessage: 'Type is required.'
          },
        },
        {
          name: 'productId',
          type: 'select',
          placeholder: 'Product',
          value:'productId',
          select: {
            type: 'api',
            api: {
              apiPath: '/products',
              method: 'GET',
              params: new HttpParams()
              .append('status', 'active')
            },
            apiDataKey:'data',
            apiValueKey: "id",
            apiViewValueKey: "name"
          },              
        }, 
        {
          name: 'status',
          type: 'select',
          placeholder: 'Status',
          value:'status',
          values: [
            { value: 'active', viewValue: 'active' },
            { value: 'inactive', viewValue: 'inactive' },
          ],
          required: {
            value: true,
            errorMessage: 'Status is required.'
          }
        },
        {
          name: "isEditable",
          type: "checkbox",
          placeholder: "Is Editable",
          value:'isEditable'
        }
      ],
      getApi:{
        apiPath: `/watch-faces/${this.watchFaceId}`,
        method:'GET'
      },
      apiDataKey:'data',
      submitApi: {
        apiPath: `/watch-faces/${this.watchFaceId}`,
        method: 'PATCH'
      },
      formButton:
      {
        type: 'raised',
        color: 'primary',
        label: 'Save'
      },
      submitSuccessMessage: "Watch faces successfully updated!"
    }
  }

  formSubmitted(event: boolean) {
    this.outputEvent.emit(event);
  }


}
