// In-Built Modules
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

// Components
import { VendorsComponent } from './components/vendors/vendors.component';
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { ConsumersComponent } from './components/consumers/consumers.component';
import { InternalsComponent } from './components/internals/internals.component';
import { SupportsComponent } from './components/supports/supports.component';
import { RolesComponent } from './components/roles/roles.component';
import { PermissionComponent } from './components/permission/permission.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { UnitsComponent } from './components/units/units.component';
import { CategoryComponent } from './components/category/category.component';
import { BuildersComponent } from './components/builders/builders.component';
import { ColorsComponent } from './components/colors/colors.component';
import { MasterProductsComponent } from './components/master-products/master-products.component';
import { PackageTypeComponent } from './components/package-type/package-type.component';
import { RoomTypeComponent } from './components/room-type/room-type.component';
import { PackagesComponent } from './components/packages/packages.component';
import { ProductsComponent } from './components/products/products.component';
import { SubCategoryComponent } from './components/sub-category/sub-category.component';
import { ConsumerDetailsComponent } from './components/consumers/consumer-details/consumer-details.component';
import { VerdorDetailsComponent } from "./components/vendors/verdor-details/verdor-details.component";
import { SupportDetailsComponent } from "./components/supports/support-details/support-details.component";
import { InternalDetailsComponent } from "./components/internals/internal-details/internal-details.component";
import { BuilderDetailsComponent } from "./components/builders/builder-details/builder-details.component";
import { ProjectDetailsComponent } from "./components/projects/project-details/project-details.component";
import { OrdersComponent } from "./components/orders/orders.component";
import { MasterProductDetailsComponent } from "./components/master-products/master-product-details/master-product-details.component";
import { OrderDetailsComponent } from "./components/orders/order-details/order-details.component";
import { ConfigurationsComponent } from "./components/configurations/configurations.component";
import { LeadsComponent } from "./components/leads/leads.component";
import { LeadsDetailsComponent } from "./components/leads/leads-details/leads-details.component";
import { CustomersComponent } from "./components/customers/customers.component";
import { CustomerDetailComponent } from "./components/customers/customer-detail/customer-detail.component";
import { ActivitiesComponent } from "./components/activities/activities.component";
import { SportsComponent } from "./components/sports/sports.component";
import { WatchFacesComponent } from "./components/watch-faces/watch-faces.component";
import { FaqComponent } from "./components/faq/faq.component";
import { MetaInfoComponent } from "./components/meta-info/meta-info.component";
import { CollectionsComponent } from "./components/collections/collections.component";
import { DetailCategoryComponent } from "./components/category/detail-category/detail-category.component";
import { OrderTransactionsComponent } from "./components/orders/order-payments/order-transactions/order-transactions.component";
import { BusinessesComponent } from "./components/businesses/businesses.component";
import { ViewBusinessComponent } from "./components/businesses/view-business/view-business.component";
/**
 * Declare App Related all routes here
 */
const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        redirectTo: "welcome",
        pathMatch: "full",
      },
      {
        path: "welcome",
        component: WelcomeComponent,
      },
      // {
      //   path:'activities',
      //   component:ActivitiesComponent
      // },
      // {
      //   path:'sports',
      //   component:SportsComponent
      // },
      // {
      //   path:'watch-faces',
      //   component:WatchFacesComponent
      // },
      {
        path : 'customers',
        component: CustomersComponent
      },

      {
        path: 'user-detail/:id',
        component: CustomerDetailComponent,
        
      },
      {
        path : 'consumer/:id',
        component: ConsumerDetailsComponent
      },
      {
        path : 'internals',
        component : InternalsComponent
      },
      {
        path : 'internal/:id',
        component : InternalDetailsComponent
      },
      // {
      //   path : 'supports',
      //   component : SupportsComponent
      // },
      // {
      //   path : 'support/:id',
      //   component : SupportDetailsComponent
      // },
      // {
      //   path : 'roles',
      //   component : RolesComponent
      // },
      // {
      //   path :'permissions',
      //   component : PermissionComponent
      // },
      
      {
        path:'orders/:id',
        component : OrderTransactionsComponent
      },
      {
        path : 'orders',
        component: OrdersComponent
      },
      {
        path : 'category',
        component: CategoryComponent
      },
        {
        path : 'category/:id',
        component: DetailCategoryComponent
      },
      // {
      //   path: 'sub-category',
      //   component: SubCategoryComponent
      // },
      {
        path : 'collections',
        component: CollectionsComponent
      },
      {
        path : 'products',
        component : ProductsComponent
      },
      {
        path :'supplier-business',
        component : BusinessesComponent
      },
      {
        path :'supplier-business/:id',
        component : ViewBusinessComponent
      }
     
         ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRouteModule {}
